FROM node:14.15.0-alpine
ENV NODE_ENV production
ENV PORT 5500

WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]

RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
#RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/testing" >> /etc/apk/repositories
RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories

# Add support for https on wget
RUN apk upgrade --available

RUN apk update && apk add --no-cache wget && apk --no-cache add openssl wget && apk add ca-certificates && update-ca-certificates

# Add phantomjs
RUN wget -qO- "https://github.com/dustinblackman/phantomized/releases/download/2.1.1a/dockerized-phantomjs.tar.gz" | tar xz -C / \
    && npm config set user 0 \
    && npm install -g phantomjs-prebuilt

# Add fonts required by phantomjs to render html correctly
RUN apk add --update ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family && rm -rf /var/cache/apk/*

RUN npm config set unsafe-perm true

RUN apk add --no-cache --virtual .gyp \
    python \
    make \
    g++ \
    git

RUN set -ex \
    && apk add musl \
    && apk add busybox \
    && apk add ncurses-libs readline \
    && apk add bash \
    #&& apk add --no-cache libressl2.7-libssl \
    #&& apk add --no-cache libsasl musl libressl2.7-libcrypto \
    && apk add --no-cache curl mongodb-tools

RUN npm install --production --silent && mv node_modules ../
RUN npm install -g pm2 --silent
COPY . .
EXPOSE 5500
CMD ["pm2-runtime", "server.js"]