require("./controllers/PassportController");
const passport = require("passport"),
    config = require("./config/config"),
    jwt = require("jwt-simple"),
    legajosController = require("./controllers/legajosController"),
    tenantsController = require("./controllers/tenantsController"),
    usersController = require("./controllers/usersController"),
    usuariosController = require("./controllers/usuariosController"),
    bigQueryController = require("./controllers/bigQueryController"),
    fileController = require("./controllers/fileController"),
    applicationsController = require("./controllers/applicationsController"),
    ThirdPartyController = require("./controllers/ThirdPartyController"),
    FreemiumController = require("./controllers/FreemiumController"),
    AuthController = require("./controllers/AuthController"),
    ThirdPartyControllerInstance = new ThirdPartyController(),
    AuthControllerInstance = new AuthController(),
    FreemiumControllerInstance = new FreemiumController(AuthControllerInstance);

// Middlewares
function sessionRedirect(req, res) {
    const { back_url } = req.session;
    if (back_url) return res.redirect(back_url);
    return res.redirect(config.back_url);
}

function appsMiddleware(req, res, next) {
    try {
        if (!req.user) return res.sendStatus(413);
        req.user.hash = config.jwt.hash;

        let userJwt = false;

        try {
            userJwt = jwt.encode(req.user, config.jwt.secret);
        } catch (e) {
            console.error(e);
            return res.sendStatus(403);
        }

        if (!userJwt) return res.sendStatus(500);
        req.user.jwt = userJwt;
    } catch (e) {
        console.error(e);
        return res.status(500).json({
            message: "Error creating jwt",
        });
    }
    next();
}

function checkAuthentication(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else res.sendStatus(403);
}

module.exports = function (app) {
    app.get("/me", checkAuthentication, (req, res) => {
        if (!req.user) return res.sendStatus(403);
        return res.json(req.user);
    });

    app.post("/files/convert", (req, res) => fileController.convert(req, res));

    app.post("/freemium/account", (req, res, next) =>
        FreemiumControllerInstance.createAccount(req, res, next)
    );
    app.post("/freemium/company", (req, res) =>
        FreemiumControllerInstance.setCompany(req, res)
    );
    app.post("/freemium/phone", (req, res) =>
        FreemiumControllerInstance.setPhone(req, res)
    );
    app.post("/freemium/tenant", (req, res) =>
        FreemiumControllerInstance.createTenant(req, res)
    );

    app.post(
        "/webhook/pipedrive",
        ThirdPartyControllerInstance.webhookPipedrive
    );

    app.get("/legajos", legajosController.getAll);
    app.get("/legajo/:id", legajosController.getLegajoById);
    app.put("/legajo/score/:_id", legajosController.setScore);
    app.put("/legajo/scores", legajosController.setScores);
    app.put("/legajo_save/:id", legajosController.saveLegajoHistory);
    app.get("/legajo_pdf/:id", legajosController.getLegajoPdfById2);
    app.delete("/legajo/:id", legajosController.deleteLegajo);
    app.post("/legajos/migrate", legajosController.migrate);
    app.post(
        "/legajo/managment/assign/:id/:action",
        legajosController.saveAssignedUsers
    );
    app.post("/legajo/status/:id", legajosController.saveStatus);

    app.get("/tenants", tenantsController.getAll);
    app.get("/tenant/:id", tenantsController.getTenantById);
    app.post("/tenant", tenantsController.saveTenant);
    app.delete("/tenant/:id", tenantsController.deleteTenant);

    app.get("/users", usersController.getAll);
    app.get("/user/:id", usersController.getUserById);
    app.post("/user", usersController.saveUser);
    app.get("/users/tenant/:tenantId", usersController.getTenantUsersById);
    app.delete("/user/:id", usersController.deleteUser);

    app.get("/usuarios", usuariosController.getAll);
    app.delete("/usuario/:id", usuariosController.deleteUser);

    app.get("/file/:bucket/:path/:file", fileController.get);
    app.get("/file", fileController.get);

    app.get(
        "/auth/google",
        passport.authenticate("google", {
            scope: ["profile", "email"],
        })
    );

    app.get(
        "/auth/facebook",
        passport.authenticate("facebook", { scope: "email" })
    );

    app.get(
        "/auth/google/callback",
        passport.authenticate("google", {
            failureRedirect: "https://admin.digiventures.la",
        }),
        sessionRedirect
    );

    app.get(
        "/auth/facebook/callback",
        passport.authenticate("facebook", {
            failureRedirect: "https://admin.digiventures.la",
        }),
        sessionRedirect
    );

    app.post("/login", (req, res, next) => {
        AuthControllerInstance.login(req, res, next);
    });

    app.get("/logout", function (req, res) {
        req.logout();
        res.send("logout");
    });

    app.post("/bigquery/:table", bigQueryController.query);
    app.get("/bigquery/tenants", bigQueryController.getTenants);

    app.get(
        "/applications/webhooks",
        appsMiddleware,
        applicationsController.getFull
    );
    app.post("/applications", appsMiddleware, applicationsController.create);
    app.put("/applications/:id", appsMiddleware, applicationsController.update);

    app.get("/migrate/users_legajos", legajosController.migrateUserLegajo);
};
