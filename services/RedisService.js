const config = require("../config/config");

class RedisService {
    constructor() {}

    get = async (key) => {
        return new Promise((resolve, reject) => {
            config.clientRedis.get(key, (err, data) => {
                if (err) return reject(err);

                return resolve(data);
            });
        });
    };

    del = async (key) => {
        return new Promise((resolve, reject) => {
            config.clientRedis.del(key, (err, data) => {
                if (err) return reject(false);

                return resolve(true);
            });
        });
    };

    set = async (key, value, expirationTime = 60000 * 60 * 12) => {
        return new Promise((resolve, reject) => {
            if (typeof value == "object") value = JSON.stringify(value);
            config.clientRedis.set(key, value, (err, reply) => {
                if (err) return reject(false);
                config.clientRedis.expire(
                    key,
                    expirationTime,
                    (err2, reply2) => {
                        if (err2) return reject(false);
                        return resolve(reply2);
                    }
                );
            });
        });
    };

    isExpired = async (key) => {
        return new Promise((resolve) => {
            config.clientRedis.ttl(key, (err, data) => {
                if (data <= -1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    };

    clean = () => {
        config.clientRedis.keys("*", (err, keys) => {
            keys.forEach((key) => {
                config.clientRedis.ttl(key, (err, data) => {
                    if (data <= -1) {
                        config.clientRedis.del(key);
                    }
                });
            });
        });
    };

    keys = () => {
        return new Promise((resolve) => {
            config.clientRedis.keys("*", (err, keys) => {
                return resolve(keys);
            });
        });
    };
}

module.exports = RedisService;
