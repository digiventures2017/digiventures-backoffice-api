const config = require("../config/config");
const utils = require("../config/utils");

module.exports = {
    async save(data, dev, old = false) {
        const baseUrl = utils.getUrlTenants(dev);
        const url = `${baseUrl}/tenant`;

        const response = await utils.fetchUrl(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                from: "onboarding-backoffice",
                old: old,
            },
            body: JSON.stringify(data),
        });

        if (response) {
            return response;
        }
        return [];
    },

    async getAll(tenantsUser, isDev) {
        const baseUrl = utils.getUrlTenants(isDev);
        const ids = tenantsUser?.length ? tenantsUser.join(",") : "";
        const url = `${baseUrl}/tenants?select=_id,name,campaign,subdomain,constants.scoringConfiguration.references,constants.stateManagement.arr${
            ids ? `&_id=${ids}` : ""
        }`;

        const response = await utils.fetchUrl(url);

        if (response) {
            return response;
        }
        return [];
    },

    async getById(_id, dev) {
        const redisKey = `tenant-${_id}-${dev}`;
        const getCache = await config.redis.get(redisKey);
        if (getCache) {
            return JSON.parse(getCache);
        }

        const baseUrl = utils.getUrlTenants(dev);
        const url = `${baseUrl}/tenant/${_id}`;
        const tenant = await utils.fetchUrl(url);
        if (tenant) {
            try {
                await config.redis.set(redisKey, JSON.stringify(tenant));
            } catch (e) {
                console.log("Error setting cache");
            }
            return tenant;
        }
        return {};
    },

    async update(_id, data, dev, old = false) {
        const baseUrl = utils.getUrlTenants(dev);
        const url = `${baseUrl}/tenant/${_id}`;

        const response = await utils.fetchUrl(url, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                from: "onboarding-backoffice",
                old: old,
            },
            body: JSON.stringify(data),
        });
        if (response) {
            const redisKey = `tenant-${_id}-${dev}`;
            await config.redis.del(redisKey);
            return response;
        }
        return {};
    },

    async delete(_id, dev) {
        const redisKey = `tenant-${_id}-${dev}`;

        const baseUrl = utils.getUrlTenants(dev);
        const url = `${baseUrl}/tenant/${_id}`;

        const tenant = await utils.fetchUrl(url, {
            method: "DELETE",
        });

        if (tenant) {
            try {
                await config.redis.del(redisKey);
            } catch (e) {
                console.log("Error setting cache");
            }
            return tenant;
        }
        return {};
    },
};
