const config = require("../config/config");
const stringify = require("stringify-keys");
const branchEnv = process.env.BRANCH_ENV;
const branchDev = process.env.BRANCH === "development";
const dev = process.env.NODE_ENV !== "production";
const Axios = require("axios");
const _ = require("lodash");

class ScoreService {
    constructor() { }

    getUrlScoring = (
        isDev,
        apiScoringVersion = config.apiScoringVersion.default
    ) => {
        if (apiScoringVersion == "v2") {
            if (branchEnv == "local") {
                return config.configLocal.urlScore;
            } else if (!dev && process.env.DEV != "development") {
                return config.configProd.urlScore;
            }
            return config.configDev.urlScore;
        } else {
            if (branchEnv == "local") {
                if (isDev) {
                    return config.configLocal.urlPublicScoring;
                } else {
                    return config.configProd.urlPublicScoring;
                }
            }

            if (dev) {
                if (!branchDev) {
                    return config.configProd.urlPublicScoring;
                } else {
                    return config.configDev.urlPublicScoring;
                }
            } else {
                if (!isDev) {
                    return config.configProd.urlInternalScoring;
                } else {
                    return config.configDev.urlInternalScoring;
                }
            }
        }
    };

    getScoringUser = async (
        data,
        isDev,
        headers,
        apiScoringVersion = config.apiScoringVersion.default
    ) => {
        const urlScoring = this.getUrlScoring(isDev, apiScoringVersion);
        const options = {
            headers: {
                "X-legajo-id": headers.legajoId || "Indefinido",
                "X-tenant-name": headers.subdomain,
                "Content-Type": "application/json",
            },
        };
        try {
            const result = await Axios.post(urlScoring, data, options);
            return result.data;
        } catch (e) {
            return e.response.data;
        }
    };

    getTypeScoring = (tenant, dataService, constantsScoring, scoring) => {
        if (dataService == "siisa") {
            const responseSplit = scoring.responseSplit;

            if (responseSplit) {
                for (const index in constantsScoring) {
                    const tmpScoring = constantsScoring[index];
                    let existScoring = true;

                    for (const parameter in tmpScoring.parameters) {
                        const value = tmpScoring.parameters[parameter];

                        if (responseSplit[parameter] != value) {
                            existScoring = false;
                        }
                    }

                    if (existScoring) {
                        return tmpScoring;
                    }
                }
            }
        } else if (dataService == "nosis") {
            const statusCode =
                _.get(scoring, "nosis.Contenido.Resultado.Estado") ||
                scoring.estado;

            for (const index in constantsScoring) {
                const tmpScoring = constantsScoring[index];

                if (tmpScoring.code == statusCode) {
                    return tmpScoring;
                }
            }
        } else if (dataService == "webflow") {
            const statusCode = _.get(scoring, "scoringId");

            for (const index in constantsScoring) {
                const tmpScoring = constantsScoring[index];
                if (tmpScoring.idScoring == statusCode) {
                    return tmpScoring;
                }
            }
        }

        const pendingSinScore = _.get(tenant, 'steps["pending"]["SinScore"]');

        if (pendingSinScore && pendingSinScore.enabled) {
            for (const index in constantsScoring) {
                const tmpScoring = constantsScoring[index];

                if (tmpScoring.type == "SinScore") {
                    return tmpScoring;
                }
            }
        }

        for (const index in constantsScoring) {
            const tmpScoring = constantsScoring[index];

            if (tmpScoring.type == "Rechazado") {
                return tmpScoring;
            }
        }
    };

    getScoring = async (tenant, legajo, data, configurationId = "") => {
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (resolve, reject) => {
            let bodyUser = {};

            const bodyLegajo = {};
            try {
                const { main, configuration } = _.get(
                    tenant,
                    "constants.scoring",
                    {
                        main: "default",
                        configuration: {
                            default: {},
                        },
                    }
                );

                if (!configurationId) configurationId = main;

                const scoringConfig = configuration[configurationId] || {};

                const {
                    actions = [
                        "set_type_scoring",
                        "read",
                        "set_managment_status",
                    ],
                } = scoringConfig;

                const dataServiceName = scoringConfig.data_service;

                let dataServiceNamePath =
                    dataServiceName === "uflow" ? "siisa" : dataServiceName;

                if (configurationId !== main)
                    dataServiceNamePath = `scoring_${configurationId}`;

                const thirdPartyPath = `thirdParty.${dataServiceNamePath}`;

                let additional_info = {};
                // NO SCORING SERVICE
                if (!dataServiceName) {
                    const typeScoring = "Directo";
                    bodyLegajo.typeScoring = typeScoring;

                    bodyLegajo.statusScoring =
                        config.typeScoringStatus[typeScoring].statusLegajo;

                    bodyLegajo.completenessLegajo =
                        config.completenessLegajo.INCOMPLETO;

                    //status assignation
                    const managment = _.get(scoringConfig, "managment", {});

                    if (
                        main === configurationId ||
                        actions.includes("set_managment_status")
                    ) {
                        for (const _typeScoring in managment) {
                            const scoringType = typeScoring
                                .normalize("NFD")
                                .replace(/[\u0300-\u036f]/g, "")
                                .split(" ")
                                .join("")
                                .toLowerCase();
                            if (
                                Object.prototype.hasOwnProperty.call(
                                    managment,
                                    _typeScoring
                                ) &&
                                _typeScoring == scoringType
                            ) {
                                bodyLegajo.status = managment[_typeScoring];
                            }
                        }

                        if (!bodyLegajo.status) {
                            bodyLegajo.status = "Nuevo";
                        }
                    }

                    return resolve({
                        legajo: bodyLegajo,
                    });
                } else {
                    let scoringConstant = scoringConfig.arr || {};

                    const oldMethod = {
                        siisa: true,
                        nosis: true,
                        lampampa: false,
                    };
                    const isOldMethod = oldMethod[scoringConfig.data_service];
                    // Seed used in UFLOW + !oldMethods
                    const seed = {
                        legajo: legajo,
                        tenant: tenant,
                    };

                    // SIISA / NOSIS
                    if (isOldMethod) {
                        const siisa = scoringConfig.siisa
                            ? scoringConfig.siisa
                            : config.defaultSiisa;

                        if (scoringConfig.data_service === "nosis") {
                            const replaceResult = this.replaceFromSeed(
                                { ...legajo, legajo, tenant },
                                siisa,
                                ["{{", "}}"]
                            ).replace(/\+/g, "");

                            const nosisData = Querystring.parse(
                                replaceResult.split("?")[1] || ""
                            );

                            additional_info = this.nosisAdditionalInformation(
                                siisa,
                                nosisData
                            );
                        } else {
                            const replaceResult = this.replaceFromSeed(
                                { ...legajo, legajo, tenant },
                                siisa,
                                ["{{", "}}"]
                            ).replace(/\+/g, "");
                            const firstIndex = siisa.indexOf("{");
                            const splitted = replaceResult
                                .slice(firstIndex)
                                .split("/");
                            for (
                                let index = 0;
                                index < splitted.length;
                                index = index + 2
                            ) {
                                const propName = splitted[index + 1];
                                const propValue = splitted[index];
                                additional_info[propName] = propValue;
                            }
                        }

                        if (scoringConstant) {
                            if (typeof scoringConstant === "string")
                                scoringConstant = JSON.parse(scoringConstant);
                            if (Array.isArray(scoringConstant))
                                scoringConstant.forEach((json, index) => {
                                    if (json) {
                                        try {
                                            scoringConstant[index] = JSON.parse(
                                                json
                                            );
                                        } catch (e) {
                                            scoringConstant[index] = json;
                                        }
                                    }
                                });
                        }
                    }
                    // UFLOW
                    else if (scoringConfig.data_service === "uflow") {
                        const additional_info_structure =
                            scoringConfig?.uflow?.parameters || {};
                        additional_info = this.setObjectData(
                            additional_info_structure,
                            seed
                        );
                    } else {
                        const additional_info_structure =
                            scoringConfig.additional_info || {};

                        additional_info = this.setObjectData(
                            additional_info_structure,
                            seed
                        );
                    }
                }
                const headers = {
                    subdomain: tenant.subdomain,
                    legajoId: legajo._id,
                };

                let body = {};

                let gender = legajo?.gender
                    ? legajo.gender[0].toUpperCase()
                    : "";

                if (scoringConfig.data_service === "lapampa" && gender) {
                    const genders = {
                        M: "MALE",
                        F: "FEMALE",
                        Masculino: "MALE",
                        Femenino: "FEMALE",
                    };
                    gender = genders[gender];
                }

                const apiScoringVersion =
                    config.apiScoringVersion[scoringConfig.data_service] ||
                    config.apiScoringVersion.default;

                if (apiScoringVersion == "v2") {
                    body = {
                        tenantId: tenant._id,
                        configurationId: configurationId,
                        dev: data.dev,
                        workflow: additional_info.workflow || "score",
                        //HARDCODED
                        customer_info: {
                            identification_number: legajo.idNumber,
                            name: legajo.firstname || legajo.name,
                            surname: legajo.lastname || ".",
                            email: legajo.email,
                            telephone_number: legajo.mobilePhone,
                            gender: gender,
                        },
                        additional_info,
                    };
                } else {
                    body = {
                        tenant: tenant._id,
                        provider: "onboarding",
                        configurationId: configurationId,
                        service: scoringConfig.data_service,
                        service_configuration_id: configurationId,
                        //HARDCODED
                        customer_info: {
                            identification_number: legajo.idNumber,
                            name: legajo.firstname || legajo.name,
                            surname: legajo.lastname || ".",
                            email: legajo.email,
                            telephone_number: legajo.mobilePhone,
                            gender: gender,
                        },
                        fingerprint: _.get(legajo, "fingerprint"),
                        additional_info,
                    };
                }

                if (scoringConfig.data_service === "webflow") {
                    body.credentials = {
                        user: scoringConfig.webflow,
                        password: scoringConfig.webflowPassword,
                    };
                }
                const scoring = await this.getScoringUser(
                    body,
                    data.dev,
                    headers,
                    apiScoringVersion
                );

                let typeScoring = {};

                if (scoring?.result?.result) {
                    const status = scoring.result.status;
                    scoring.result = scoring.result.result;
                    scoring.status = status;
                }

                if (
                    !["siisa", "uflow", "nosis", "webflow"].includes(
                        scoringConfig.data_service
                    )
                ) {
                    try {
                        const scoringConstant = scoringConfig.arr;
                        scoringConstant.map((codes, i) => {
                            if (
                                scoring?.result?.code &&
                                codes.includes(scoring.result.code)
                            ) {
                                scoring.result.scoring_type =
                                    config.typeScoring[i];
                            }
                        });
                    } catch (e) {
                        console.error(e);
                    }
                }

                typeScoring = {
                    type: scoring.result.scoring_type,
                };

                if (scoring.status == "ERROR") {
                    typeScoring = {
                        type: "SinScore",
                    };
                }

                if (!typeScoring.type) {
                    typeScoring.type = "SinScore";
                }

                //status assignation
                const managment = scoringConfig?.managment || {};

                if (
                    main === configurationId ||
                    actions.includes("set_managment_status")
                ) {
                    for (const _typeScoring in managment) {
                        const scoringType = typeScoring.type
                            .normalize("NFD")
                            .replace(/[\u0300-\u036f]/g, "")
                            .split(" ")
                            .join("")
                            .toLowerCase();
                        if (
                            Object.prototype.hasOwnProperty.call(
                                managment,
                                _typeScoring
                            ) &&
                            _typeScoring == scoringType
                        ) {
                            bodyLegajo.status = managment[_typeScoring];
                        }
                    }
                    if (!bodyLegajo.status) {
                        bodyLegajo.status = "Nuevo";
                    }
                }

                if (actions.includes("set_type_scoring")) {
                    bodyLegajo.typeScoring = typeScoring.type;
                    bodyLegajo.statusScoring =
                        config.typeScoringStatus[typeScoring.type].statusLegajo;

                    bodyLegajo.completenessLegajo =
                        config.completenessLegajo.INCOMPLETO;
                }

                if (["siisa", "uflow"].includes(dataServiceName)) {
                    bodyUser[thirdPartyPath] = {
                        firstName: _.get(
                            scoring,
                            "result.scoring_info.first_name"
                        ),
                        lastName: _.get(
                            scoring,
                            "result.scoring_info.last_name"
                        ),
                        fullName: _.get(
                            scoring,
                            "result.scoring_info.extraValues.siisa_apellidoNombre"
                        ),
                        taxIdNumber: _.get(scoring, "result.scoring_info.cuil"),
                        gender: _.get(
                            scoring,
                            "result.scoring_info.extraValues.siisa_sexo"
                        ),
                        birthDate: _.get(
                            scoring,
                            "result.scoring_info.birthdate"
                        ),
                        address: _.get(scoring, "result.scoring_info.address"),
                        policyResult: _.get(
                            scoring,
                            "result.scoring_info.extraValues.decisionResult"
                        ),
                    };

                    const variablesSiisa = {};

                    const extraValues = _.get(
                        scoring,
                        "result.scoring_info.extraValues"
                    );

                    if (extraValues) {
                        Object.keys(extraValues).map((item) => {
                            const value = extraValues[item];

                            variablesSiisa[item] = {
                                key: item,
                                value: value,
                            };
                        });
                    }

                    bodyLegajo[thirdPartyPath] = {
                        policyResult: bodyUser[thirdPartyPath].policyResult,
                        response: {
                            "Score SIISA": _.get(
                                scoring,
                                "result.scoring_info.extraValues.siisa_score"
                            ),
                            "categoría monot": _.get(
                                scoring,
                                "result.scoring_info.extraValues.siisa_monotributoCategoria"
                            ),
                            Perfil: _.get(
                                scoring,
                                "result.scoring_info.profile"
                            ),
                            status: _.get(
                                scoring,
                                "result.scoring_info.extraValues.status"
                            ),
                            maxCapitalScoring: _.get(
                                scoring,
                                "result.scoring_info.extraValues.maxCapitalScoring"
                            ),
                            producto: _.get(
                                scoring,
                                "result.scoring_info.extraValues.producto"
                            ),
                        },
                        variables: variablesSiisa,
                        v2: scoring,
                    };
                } else if (dataServiceName == "nosis") {
                    bodyUser = {
                        [thirdPartyPath]: {
                            result: scoring.result,
                        },
                    };

                    bodyLegajo[thirdPartyPath] = {
                        result: scoring.result,
                    };
                } else {
                    bodyUser[thirdPartyPath] = scoring;
                    bodyLegajo[thirdPartyPath] = scoring;
                }
                bodyLegajo.simulationProduct = this.getProduct(
                    bodyLegajo,
                    scoringConfig.data_service
                );
                return resolve({
                    legajo: bodyLegajo,
                    thirdPartyPath,
                });
            } catch (e) {
                console.error(e);
                return reject(false);
            }
        });
    };

    getProduct = (response, service) => {
        let product = "A";
        switch (service) {
            case "siisa":
                product = _.get(
                    response,
                    "thirdParty.siisa.response.producto",
                    "A"
                );
                break;
        }
        return product;
    };

    setObjectData(parameters, seed) {
        if (!parameters) parameters = {};
        const keys = stringify(parameters);
        let obj = {};
        keys.map((k) => {
            const value = this.replaceFromSeed(seed, _.get(parameters, k));
            obj = _.set(obj, k, value);
        });
        return obj;
    }

    replaceFromSeed = (data, text) => {
        if (!text) return "";
        if (typeof text != "string") return text;
        const matches = text.match(new RegExp("{(.*?)}+", "g"));
        if (matches) {
            matches.map((m) => {
                let key = m.replace(/{|}/g, "");
                if (key.includes("customInput")) {
                    const search = key.split("|");
                    key = this.getCustomInputIndex(data, search);
                }
                let value = _.get(data, key);
                if (typeof value == "undefined") value = "";
                text = text.replace(m, value);
            });
        }
        return text;
    };

    getCustomInputIndex(data, search) {
        const dataProp = search[0];
        const arr = _.get(data, dataProp, []);
        let key = false;
        arr.map((obj, i) => {
            if (obj.name == search[1]) {
                const prop = search[2] || "value";
                key = `${dataProp}[${i}].${prop}`;
            }
        });
        return key;
    }

    getIndicesOf = (searchStr, str, caseSensitive) => {
        const searchStrLen = searchStr.length;
        if (searchStrLen == 0 || !str) {
            return [];
        }
        let startIndex = 0,
            index;

        const indices = [];
        if (str && !caseSensitive) {
            str = str.toLowerCase();
            searchStr = searchStr.toLowerCase();
        }

        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    };

    tryParseJSON(jsonString) {
        let jsonValue = jsonString;
        if (jsonString.startsWith("") && jsonString.endsWith("")) {
            jsonValue = jsonString.substring(1, jsonString.length - 1);
        }
        try {
            const o = JSON.parse(jsonValue);
            if (o && typeof o === "object") {
                return o;
            }
        } catch (e) {
            return jsonString;
        }

        return jsonString;
    }

    nosisAdditionalInformation(url, body) {
        const values = url.split("&");
        const obj = values.reduce((prev, curr) => {
            const [prop, value] = curr.split("=");
            if (value.startsWith("{{") && value.endsWith("}}")) {
                prev[value.replace(/[{{}}]/g, "")] = body[prop];
            }
            return prev;
        }, {});

        return obj;
    }
}

module.exports = ScoreService;
