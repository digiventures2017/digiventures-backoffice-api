const config = require("../config/config"),
    Utils = require("../config/utils"),
    _ = require("lodash"),
    bCrypt = require("bcrypt-nodejs"),
    fetch = require("node-fetch"),
    AdminUsers = require("../models/adminUsers"),
    TenantService = require("../services/TenantService");

exports.getAll = function (req, res) {
    const { name, email } = req.query;
    let { tenants: selectedTenants, role, sorting = "" } = req.query;

    let stringQuery = "";

    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (
        req.user.role != "SuperAdministrador" &&
        req.user.role != "Administrador"
    ) {
        return;
    }

    const perPage = 20;
    let page = req.query.page || 0;

    if (page >= 1) {
        page--;
    }

    let query = {
        test: req.user.test,
    };

    if (req.user.tenants.length) {
        query.tenants = { $in: req.user.tenants };
    }

    if (sorting) {
        stringQuery += "sorting=" + sorting + "&";
        sorting = sorting.split(",").map((s) => {
            const sv = s.split(":");
            return sv
        });
    }

    if (selectedTenants) {
        const tmpSelectedTenants = selectedTenants.split(",");
        query.tenants = { $in: tmpSelectedTenants };
        stringQuery += "tenants=" + selectedTenants + "&";
    }

    if (role) {
        const tmpSelectedRoles = role.split(",");
        query.role = { $in: tmpSelectedRoles };
        stringQuery += "role=" + role + "&";
    }

    if (name) {
        query.$text = {
            $search: name,
            $language: "es",
            $caseSensitive: false,
        };
        stringQuery += "name=" + name + "&";
    }

    if (email) {
        query.email = email;
        stringQuery += "email=" + email + "&";
    }

    const findUsers = AdminUsers.find(query)
        .limit(perPage)
        .skip(perPage * page)
        .select("_id name email role")
        .sort(sorting)
        .exec();

    const countUsers = AdminUsers.find(query).countDocuments().exec();
    Promise.all([findUsers, countUsers]).then((values) => {
        let result = {
            results: values[0],
            count: values[1],
            page: page,
            perPage: perPage,
            query: query,
            stringQuery: stringQuery,
        };
        res.json(result);
    });
};

exports.getUserById = function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (
        req.user.role != "SuperAdministrador" &&
        req.user.role != "Administrador"
    ) {
        return;
    }

    let userResult = {
        name: "",
        email: "",
        role: "Comercial",
        tenants: "",
    };

    if (req.params.id != "new") {
        userResult = AdminUsers.findOne({ _id: req.params.id })
            .select(
                "_id name email role tenants legajosApproved legajosCompleted permissions"
            )
            .exec();
    }

    const tenantResult = TenantService.getAll(req.user.tenants, req.user.test);

    Promise.all([userResult, tenantResult]).then((values) => {
        const tmpUser = values[0];
        const tenants = values[1];

        tmpUser.password = "";

        const result = {
            results: tmpUser,
            tenants: tenants,
            role: req.user.role,
        };

        res.json(result);
    });
};

exports.saveUser = function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (
        req.user.role != "SuperAdministrador" &&
        req.user.role != "Administrador"
    ) {
        return;
    }

    let id = req.body._id,
        name = req.body.name,
        email = req.body.email,
        password = req.body.password,
        role = req.body.role,
        tenants = req.body.tenants,
        legajosApproved = req.body.legajosApproved,
        legajosCompleted = req.body.legajosCompleted,
        permissions = req.body.permissions || [];

    let user = {};

    user.name = name;
    user.email = email;

    if (password) {
        user.password = generateHash(password);
    }

    user.role = role;
    user.tenants = tenants;
    user.test = req.user.test;
    user.legajosApproved = legajosApproved;
    user.legajosCompleted = legajosCompleted;
    user.permissions = permissions;

    if (id) {
        user._id = id;
    }

    upsert(AdminUsers, user, function (err, result) {
        if (err) {
            return res.json({
                error: true,
                message: "Error al guardar user.",
            });
        }

        return res.json({
            id: result._id,
            message: "Datos guardado correctamente.",
        });
    });
};

exports.deleteUser = function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const idUser = req.params.id;

    if (
        req.user.role != "SuperAdministrador" &&
        req.user.role != "Administrador" &&
        !idUser
    ) {
        return;
    }

    AdminUsers.deleteOne({ _id: idUser })
        .then((result) => {
            return res.json({
                deleted: true,
            });
        })
        .catch((err) => {
            console.log("ERR CATCH: " + new Date().toString());
            console.log(err);
            console.log("-------------------------------------------");
        });
};

exports.getTenantUsersById = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (
        !req.params.tenantId ||
        (!req.user.tenants.includes(req.params.tenantId) &&
            req.user.role != "SuperAdministrador")
    ) {
        return res.sendStatus(403);
    }

    const query = {};

    query.tenants = { $in: req.params.tenantId.split(",") };

    const findUsers = await AdminUsers.find(query)
        .select("_id name email role")
        .exec();

    return res.json(findUsers);
};

const upsert = function (model, data, f) {
    if (!data._id) {
        model.create(data, f);
    } else {
        const id = data._id;
        delete data._id;
        model.findOneAndUpdate({ _id: id }, data, f);
    }
};

function generateHash(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
}
