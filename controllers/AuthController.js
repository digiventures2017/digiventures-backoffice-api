const passport = require("passport");
const bCrypt = require("bcrypt-nodejs");
const test = !(process.env.NODE_ENV === `production`);

const options = {
    lean: true,
    new: true,
};

class AuthController {
    constructor(adminUsersModel) {
        this.adminUsersModel = adminUsersModel;
    }

    async local(req, username, password, done) {
        const data = await this.adminUsersModel.findOne({
            email: username,
            test: req.body.test,
        });
        if (!data) {
            return done(null, {
                error: true,
                message: "Usuario o contraseña incorrecta.",
            });
        }

        if (!this.isValidPassword(data.password, password)) {
            return done(null, {
                error: true,
                message: "Usuario o contraseña incorrecta.",
            });
        }

        data.password = "";
        data.test = req.body.test;

        return done(null, data);
    }

    async google(req, profile) {
        const google = {
            "google.id": profile.id,
        };
        const email = profile.email;

        const userFound = await this.adminUsersModel
            .findOne({ ...google, test })
            .select("-password")
            .exec();
        if (userFound) return userFound;

        const userFoundByEmail = await this.adminUsersModel
            .findOne({ email: email, test })
            .select("-password")
            .exec();
        if (userFoundByEmail) {
            const updated = await this.adminUsersModel
                .findByIdAndUpdate(userFoundByEmail._id, google, options)
                .exec();
            delete updated.password;
            return updated;
        }

        const user = {
            ...google,
            "google.domain": profile._json.domain,
            name: `${profile.given_name} ${profile.family_name}`,
            verified: profile.verified,
            language: profile.language,
            email: profile.email,
            picture: profile.picture,
            role: "Administrador",
            test: test,
        };

        const created = await new this.adminUsersModel(user).save();
        return created;
    }

    async facebook(req, profile, done) {
        const facebook = {
            "facebook.id": profile.id,
        };
        const email = profile._json.email;

        const userFound = await this.adminUsersModel
            .findOne({ ...facebook, test })
            .select("-password")
            .exec();
        if (userFound) return done(null, userFound);

        const userFoundByEmail = await this.adminUsersModel
            .findOne({ email: email, test })
            .select("-password")
            .exec();
        if (userFoundByEmail) {
            const updated = await this.adminUsersModel
                .findByIdAndUpdate(userFoundByEmail._id, facebook, options)
                .exec();
            delete updated.password;
            return done(null, updated);
        }

        const user = {
            ...facebook,
            name: `${profile._json.first_name} ${profile._json.last_name}`,
            email: email,
            role: "Administrador",
            test: test,
        };

        const created = await new this.adminUsersModel(user).save();
        return done(null, created);
    }

    login(req, res, next) {
        passport.authenticate("local", (err, user) =>
            this.setUserLogIn(err, user, req, res, next)
        )(req, res, next);
    }

    setUserLogIn(err, user, req, res, next) {
        if (err) {
            return next(err);
        }

        if (!user._id) {
            return res.json(user);
        }

        req.logIn(user, (err) => {
            if (err) {
                return next(err);
            }

            return res.json(user);
        });
    }

    generateHash(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
    }

    isValidPassword(userpass, password) {
        return bCrypt.compareSync(password, userpass);
    }
}

module.exports = AuthController;
