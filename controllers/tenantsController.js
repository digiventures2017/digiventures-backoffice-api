const config = require("../config/config"),
    Utils = require("../config/utils"),
    _ = require("lodash"),
    fetch = require("node-fetch"),
    constants = require("../config/constants"),
    TenantService = require("../services/TenantService");

exports.getAll = async function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }

    const { tenants, test } = req.user;
    const tenantsList = await TenantService.getAll(tenants, test);

    return res.json(tenantsList);
};

exports.getTenantById = async function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }
    const tenantResult = await TenantService.getById(
        req.params.id,
        req.user.test
    );
    return res.json(tenantResult);
};

exports.deleteTenant = async function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }

    const _id = req.params.id;

    if (_id) {
        try {
            const responseV2 = await TenantService.delete(_id, req.user.test);
        } catch (e) {
            console.log(e);
        }

        res.json({
            deleted: true,
        });
    }
};

exports.saveTenant = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }

    const id = req.body.id;
    let tenant = req.body.tenant;

    const arImages = [];

    let tJson = JSON.stringify(tenant);
    const bases64 = tJson.match(/data:image\/[^;]+;base64[^"]+/g);
    if (bases64) {
        /* bases64 = [...new Set(bases64)]; */
        bases64.forEach((base64) => {
            arImages.push(Utils.putBase64ImageS3(base64, base64));
        });
        const uploaded = await Promise.all(arImages);
        if (uploaded && uploaded.length) {
            uploaded.forEach((upload) => {
                tJson = tJson.replace(upload.key, upload.url);
            });
        }
        try {
            tenant = JSON.parse(tJson);
        } catch (e) {
            console.log(e);
        }
    }

    if (id) {
        tenant._id = id;
    }

    /* const test = req.user.test;
    const dev = test ? "dev" : ""; */
    if (tenant._id) {
        try {
            const result = await TenantService.update(
                tenant._id,
                tenant,
                req.user.test
            );
            /* const response = await fetch(
                urlConfigTenant + "multitenant_configuration?id=" + tenant._id,
                {
                    method: "PUT",
                    body: JSON.stringify(tenant),
                }
            );
            const result = await response.json();

            if (constants.scoringReferences[tenant._id]) {
                delete constants.scoringReferences[tenant._id];
            } */

            /*             const redisKey = `tenants-${dev}`;
            const deleteCache = await config.redis.del(redisKey);

            const redisKeyList = `tenants-list-${dev}`;
            const deleteListCache = await config.redis.del(redisKeyList);

            const redisKeyId = `tenant-${tenant._id}-${dev}`;
            const deleteIdCache = await config.redis.del(redisKeyId);

            const urlMiddle = Utils.getUrlMiddle(req.user.test);
            const bodyCleanCache = {
                dev: req.user.test,
                tenantId: result._id,
                subdomain: result.subdomain,
                campaign: result.campaign,
            };

            fetch(urlMiddle + "/cleancache_tenant", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(bodyCleanCache),
            }); */

            let message = "Datos guardado correctamente.";

            if (result.message) {
                message = result.message;
            }

            res.json({
                id: tenant._id,
                message: message,
            });
        } catch (err) {
            console.log("ERR CATCH: " + new Date().toString());
            console.log(err);
            console.log("-------------------------------------------");
        }
    } else {
        try {
            const result = await TenantService.save(tenant, req.user.test);

            /*             const dev = req.user.test ? "dev" : "";

            const redisKey = `tenants-${dev}`;
            const deleteCache = await config.redis.del(redisKey);

            const redisKeyList = `tenants-list-${dev}`;
            const deleteListCache = await config.redis.del(redisKeyList); */

            res.json({
                id: result._id,
                message: "Datos guardado correctamente.",
            });
        } catch (err) {
            console.log("ERR CATCH: " + new Date().toString());
            console.log(err);
            console.log("-------------------------------------------");
        }
    }
};
