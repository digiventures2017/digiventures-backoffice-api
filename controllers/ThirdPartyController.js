const moment = require("moment");
const utils = require("../config/utils");
const _ = require("lodash");

const pipedriveConfiguration = {
    "creditosreconquista.pipedrive.com": {
        pipelines: {
            1: {
                stages: {
                    "14": { title: "DIRECTO", status: "Nuevo" },
                    "11": { title: "REVISION MANUAL", status: "Nuevo" },
                    "7": { title: "RECHAZADO", status: "Nuevo" },
                    "11": { title: "PENDING", status: "Nuevo" },
                    "12": { title: "RENOVACIONES", status: "Nuevo" },
                    "16": { title: "GCBA", status: "Nuevo" },
                    "2": { title: "RECONTACTAR", status: "Contactado" },
                    "5": { title: "EN GESTIÓN", status: "Contactado" },
                    "9": { title: "DESESTIMADO", status: "Desestimado" },
                    "15": { title: "TRANSFERIR", status: "Transferir" },
                    "6": { title: "REALIZADO NUEVO", status: "Liquidado" },
                    "13": {
                        title: "REALIZADO RENOVACIONES",
                        status: "Liquidado"
                    }
                },
                properties: {}
            }
        },
        dev: false
    }
};

class ThirdPartyController {
    constructor() {}

    async webhookPipedrive(req, res) {
        const { body } = req;
        const host = _.get(body, "meta.host");
        if (!host) return res.sendStatus(304);
        let data = {};
        let status = 200;
        try {
            if (body.meta.object === "deal") {
                const configuration = pipedriveConfiguration[host];
                if (!configuration) throw false;
                const pipeline =
                    configuration.pipelines[body.current.pipeline_id];
                if (!pipeline) throw false;
                data = {
                    status: pipeline.stages[body.current.stage_id].status,
                    dateUpdatedStatus: moment(),
                    external: {
                        pipedrive: {
                            id: body.current.id,
                            creator_user_id: body.current.creator_user_id,
                            user_id: body.current.user_id,
                            person_id: body.current.person_id,
                            org_id: body.current.org_id,
                            stage: {
                                id: body.current.stage_id,
                                title:
                                    pipeline.stages[body.current.stage_id]
                                        .title,
                                change_time: body.current.stage_change_time
                            }
                        }
                    }
                };

                try {
                    const update = await utils.updateLegajo(
                        data.external.pipedrive.id,
                        data,
                        configuration.dev,
                        "dealId",
                        true
                    );
                    if (update.status == 200 || update.status == 404) {
                        status = 200;
                    }
                } catch (e) {
                    console.log(e);
                    status = 500;
                }
            }
        } catch (e) {
            if (!e) status = 200;
            else status = 500;
        }

        return res.sendStatus(status);
    }
}

module.exports = ThirdPartyController;
