const axios = require("axios");
const moment = require("moment");
const get = require("lodash/get");
const bCrypt = require("bcrypt-nodejs");
const sendgrid = require("@sendgrid/mail");
const AdminUsers = require("../models/adminUsers");
const { putBase64ImageS3 } = require("../config/utils");
const { sendgrid: configSendgrid } = require("../config/config");

const hsApiKey = "eb92c501-1516-4fbf-baf0-2b070571d56f";

moment.locale("es");
sendgrid.setApiKey(configSendgrid.apiKey);
const test = !(process.env.NODE_ENV === `production`);
const headers = { headers: { "Content-Type": "application/json" } };

class FreemiumController {
    constructor(authController) {
        this.authController = authController;
    }

    async createAccount(req, res, next) {
        const { body } = req;
        const alreadyExists = await AdminUsers.findOne({ email: body.email });
        if (alreadyExists)
            return res.status(409).json({ reason: "Email unavailable" });
        const user = { ...body };
        user.name = `${user.firstname} ${user.lastname}`;
        user.test = test;
        user.role = "Administrador";
        if (user.password !== user.confirmPassword)
            return res.status(409).json({ reason: "Passwords don't match" });
        delete user.confirmPassword;
        user.password = generateHash(body.password);
        try {
            const hsData = this.setHubSpotUser(user);

            user.hubspot = { id: hsData.vid };

            const created = await new AdminUsers(user).save();
            this.authController.setUserLogIn(null, created, req, res, next);
        } catch (e) {
            console.error(e);
            return res.sendStatus(500);
        }
    }

    async setCompany(req, res) {
        const { body: company } = req;
        let { user } = req;
        try {
            user = await this.setHubSpotUser(user);
            const vid = await this.setHubSpotCompany(
                user?.company?.hubspot?.id,
                company,
                user
            );
            company.hubspot = { id: vid };

            const updated = await AdminUsers.updateOne(
                { _id: req.user._id },
                { company: company, hubspot: user.hubspot }
            );
            if (!updated) return res.sendStatus(400);
            return res.sendStatus(200);
        } catch (e) {
            console.error(e);
            return res.sendStatus(500);
        }
    }

    async setPhone(req, res) {
        const { body, user } = req;
        const { phone } = body;
        try {
            const hsBody = {
                properties: [
                    { property: "email", value: user.email },
                    {
                        property: "firstname",
                        value: user.firstname,
                    },
                    { property: "lastname", value: user.lastname },
                    { property: "mobilePhone", value: body.phone },
                    { property: "phone", value: body.phone },
                    { property: "freemium_sign_up", value: 1 },
                ],
            };
            await axios.post(
                `https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/${user.email}/?hapikey=${hsApiKey}`,
                hsBody,
                headers
            );
            const updated = await AdminUsers.updateOne(
                { _id: req.user._id },
                { phone: phone }
            );
            if (!updated) return res.sendStatus(400);
            return res.sendStatus(200);
        } catch (e) {
            console.error(e);
            return res.sendStatus(500);
        }
    }

    async createTenant(req, res) {
        if (!req.user) return res.sendStatus(401);
        const { _id } = req.user;
        let body = JSON.stringify(req.body);
        const base64Found = body.match(/(data:image\/[^;]+;base64[^"]+)/g);
        const uploads = [];
        if (base64Found) {
            base64Found.forEach((base64) =>
                uploads.push(putBase64ImageS3(base64, base64))
            );
        }
        const uploaded = await Promise.all(uploads);
        uploaded.forEach(
            (upload) => (body = body.replace(upload.key, upload.url))
        );
        body = JSON.parse(body);
        const user = await AdminUsers.findById(_id);
        const { firstname, lastname, email, company, phone, hubspot } = user;
        let { name } = user;
        if (!name && (firstname || lastname)) {
            name = [firstname, lastname].join(" ");
        }
        const userData = {
            _id,
            name,
            firstname,
            lastname,
            email,
            company,
            phone,
        };

        const userHTML = createHTML(userData);
        const tenantHTML = createHTML(body);
        const HTML = `<h3>Usuario solicitante:</h3>${userHTML}<br/><h3>Configuración</h3>${tenantHTML}`;

        const userText = createText(userData);
        const tenantText = createText(body);
        const TEXT = `Usuario solicitante:\n${userText}\n\nConfiguración\n${tenantText}`;

        const msg = {
            to: configSendgrid.to,
            from: configSendgrid.from,
            subject: "Nuevo tenant solicitado",
            html: HTML,
        };

        const dealId = await this.createDeal(
            { hubspot, ...userData },
            body,
            TEXT
        );

        if (!user?.company?.hubspot?.dealId) {
            await AdminUsers.updateOne(
                { _id: req.user._id },
                { "company.hubspot.dealId": dealId }
            );
        }

        try {
            await sendgrid.send(msg);
            try {
                await this.setHubSpotUser(user, [
                    { property: "freemium_sign_up", value: 1 },
                    { property: "freemium_configuration", value: TEXT },
                    { property: "website", value: body.domain },
                ]);
            } catch (e) {
                console.error(e);
            }
        } catch (e) {
            console.error(e);
        }
        return res.sendStatus(200);
    }

    async createDeal(user, tenant, TEXT = "") {
        const hsBody = {
            associations: {
                associatedCompanyIds: [user?.company?.hubspot?.id],
                associatedVids: [user?.hubspot?.id],
            },
            properties: [
                {
                    value: ` ${user?.company?.name || tenant.domain} ${
                        tenant.country
                    } ${tenant.selectedFlow}`,
                    name: "dealname",
                },
                {
                    value: "12053965",
                    name: "dealstage",
                },
                {
                    value: "12053964",
                    name: "pipeline",
                },
                {
                    value: +new Date(user.company.implementationDate),
                    name: "closedate",
                },
                {
                    value: "0",
                    name: "amount",
                },
                {
                    value: "newbusiness",
                    name: "dealtype",
                },
                {
                    value: TEXT,
                    name: "freemium_configuration",
                } /* JSON.stringify(tenant, null, 2), name: "json" } */,
            ],
        };
        const hsDeal = await axios.post(
            `https://api.hubapi.com/deals/v1/deal?hapikey=${hsApiKey}`,
            hsBody,
            headers
        );

        const companyId = user?.company?.hubspot?.id;
        const hubspotUserId = user?.hubspot?.id;
        const { dealId } = hsDeal.data;

        if (dealId) {
            try {
                if (hubspotUserId) {
                    await axios.put(
                        `https://api.hubapi.com/deals/v1/deal/${dealId}/associations/CONTACT?id=${hubspotUserId}&hapikey=${hsApiKey}`,
                        hsBody,
                        headers
                    );
                }
            } catch (e) {
                console.error(e);
            }
            try {
                if (companyId) {
                    await axios.put(
                        `https://api.hubapi.com/deals/v1/deal/${dealId}/associations/COMPANY?id=${companyId}&hapikey=${hsApiKey}`,
                        hsBody,
                        headers
                    );
                }
            } catch (e) {
                console.error(e);
            }
        }

        return dealId;
    }

    async setHubSpotUser(user, customData = []) {
        const { name } = user;
        let { firstname, lastname } = user;
        if ((!firstname || !lastname) && name) {
            const splitted = user.name.split(" ");
            firstname = splitted[0];
            splitted.splice(0, 1);
            lastname = splitted.join(" ");
        }
        const hsBody = {
            properties: [
                { property: "email", value: user.email },
                { property: "firstname", value: firstname },
                { property: "lastname", value: lastname },
                ...customData,
            ],
        };

        const hsContact = await axios.post(
            `https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/${user.email}/?hapikey=${hsApiKey}`,
            hsBody,
            headers
        );

        user.hubspot = { id: hsContact.data.vid };

        return user;
    }

    async setHubSpotCompany(vid, company, user) {
        const domain = (user.email || "").match(/(?<=@)[^.]+.*/g)?.[0];
        const regex = /live|hotmail|outlook|aol|yahoo|rocketmail|gmail|gmx/g;

        if (!vid) {
            const hsBody = {
                filterGroups: [
                    {
                        filters: [
                            {
                                value: company.name,
                                propertyName: "name",
                                operator: "EQ",
                            },
                        ],
                    },
                ],
                properties: ["name, vid"],
            };

            if (domain && !domain.match(regex)) {
                hsBody.filterGroups[0] = {
                    filters: [
                        {
                            value: domain,
                            propertyName: "domain",
                            operator: "EQ",
                        },
                    ],
                };
            }

            const hsCompanySearch = await axios.post(
                `https://api.hubapi.com/crm/v3/objects/companies/search?hapikey=${hsApiKey}`,
                hsBody,
                headers
            );

            const { data: companiesResuts } = hsCompanySearch;
            if (company.name && companiesResuts?.results?.length) {
                companiesResuts.results.forEach((hbCompany) => {
                    if (
                        hbCompany.properties.name.toLowerCase() ===
                        company.name.toLowerCase()
                    ) {
                        vid = hbCompany.id;
                    }
                });
            }
        }

        if (!vid) {
            const hsNewCompanyBody = {
                properties: [
                    {
                        name: "name",
                        value: company.name,
                    },
                    {
                        name: "description",
                        value: company.field,
                    },
                ],
            };

            if (domain) {
                hsNewCompanyBody.properties.push({
                    name: "domain",
                    value: domain,
                });
            }
            const hsCompanyCreated = await axios.post(
                `https://api.hubapi.com/companies/v2/companies?hapikey=${hsApiKey}`,
                hsNewCompanyBody,
                headers
            );
            if (hsCompanyCreated.data.companyId) {
                vid = hsCompanyCreated.data.companyId;
            }
        }

        if (vid) {
            await axios.put(
                `https://api.hubapi.com/companies/v2/companies/${vid}/contacts/${user.hubspot.id}?hapikey=${hsApiKey}`,
                {},
                headers
            );
        }
        return vid;
    }
}

function generateHash(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
}

function createHTML(data, prefix = "") {
    const result = Object.keys(data).map((key) => {
        let value = get(data, key);
        if (
            value !== null &&
            typeof value === "object" &&
            !(value instanceof Date)
        ) {
            value = createHTML(value, `${key}.`);
        } else if (value instanceof Date) {
            value = moment(value).format("LL");
        }
        const translation = translations[`${prefix}${key}`];
        if (translation) return `<div>${translation}: ${value}</div>`;
    });
    return `\n${result.join("\n")}`;
}

function createText(data, prefix = "") {
    const result = Object.keys(data).map((key) => {
        let value = get(data, key);
        if (
            value !== null &&
            typeof value === "object" &&
            !(value instanceof Date)
        ) {
            value = createText(value, `${key}.`);
        } else if (value instanceof Date) {
            value = moment(value).format("LL");
        }
        const translation = translations[`${prefix}${key}`];
        if (translation) return `${translation}: ${value}\n`;
    });
    return `\n${result.join("\n")}`;
}

const translations = {
    _id: "ID",
    name: "Nombre",
    // firstname: "Nombre",
    // lastname: "Apellido",
    email: "Correo electrónico",
    company: "Datos empresariales",
    phone: "- teléfono",
    "company.name": "- nombre",
    "company.field": "- industria",
    "company.country": "- país",
    "company.implmentationDate": "- fecha de implementación",
    "company.findoctorMails": "- recibir emails de FinDoctor",
    "company.contract": "- tyc",
    selectedFlow: "Tipo de flujo",
    domain: "Subdominio",
    country: "País",
    palette: "Paleta",
    favicon: "Favicon",
    logo: "Logo",
    bannerImg: "Landing: fondo de pantalla",
    navbarText: "Navbar: texto",
    textPrimary: "Landing: texto principal",
    textSecondary: "Landing: texto secundario",
    buttonText: "Landing: texto botón siguiente",
    termsAndConditions: "Términos y condiciones",
    pageDescription: "Descripción de la página",
    "palette.buttonBackground": "- color primario del botón",
    "palette.navbarBackground": "- color primario del fondo",
    "palette.navbarText": "- color primario del texto del navbar",
    "palette.buttonText": "- color primario del texto del botón",
};

module.exports = FreemiumController;
