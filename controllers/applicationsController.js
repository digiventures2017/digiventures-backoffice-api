const Utils = require("../config/utils");
const TenantService = require("../services/TenantService");

exports.getFull = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const query = Utils.query(req.query);

    const applicationsUrl =
        Utils.getUrlApplications() + `/backoffice/webhook${query}`;
    const applications = Utils.fetchUrl(applicationsUrl, {
        headers: { jwt: req.user.jwt },
    });
    const tenants = TenantService.getAll(req.user.tenants, req.user.test);

    Promise.all([applications, tenants])
        .then((values) => {
            const applicationsData = values[0] || {};
            applicationsData.tenants = (values[1] || []).map((tenant) => ({
                value: tenant._id,
                text: tenant.name,
            }));
            return res.json(applicationsData);
        })
        .catch((e) => {
            console.log(e);
            return res.status(500).json({
                message: "Error getting data",
            });
        });
};

exports.create = async (req, res) => {
    const url = Utils.getUrlApplications() + `/backoffice/application`;
    try {
        const result = await Utils.fetchUrl(url, {
            headers: { jwt: req.user.jwt, "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify(req.body),
        });
        return res.status(201).json(result);
    } catch (e) {
        return res.status(500).json({
            message: "Error getting data",
        });
    }
};

exports.update = async (req, res) => {
    const url =
        Utils.getUrlApplications() + `/backoffice/application/${req.params.id}`;
    try {
        const result = await Utils.fetchUrl(url, {
            headers: { jwt: req.user.jwt, "Content-Type": "application/json" },
            method: "PUT",
            body: JSON.stringify(req.body),
        });
        return res.status(200).json(result);
    } catch (e) {
        return res.status(500).json({
            message: "Error getting data",
        });
    }
};
