const Utils = require("../config/utils");

const { extend } = require("lodash");
const SimpleMarkdown = require("simple-markdown");
const HtmlPdf = require("html-pdf");
/* const removeMd = require("remove-markdown"); */

const mdToHtml = (value) => {
    if (!value) return "";
    const underlineRule = {
        order: SimpleMarkdown.defaultRules.em.order - 0.5,

        match: function (source) {
            return /^\+\+([\s\S]+?)\+\+(?!_)/.exec(source);
        },

        parse: function (capture, parse, state) {
            return {
                content: parse(capture[1], state),
            };
        },
        html: function (node, output) {
            return "<u>" + output(node.content) + "</u>";
        },
    };

    const rules = extend({}, SimpleMarkdown.defaultRules, {
        underline: underlineRule,
    });

    const rawBuiltParser = SimpleMarkdown.parserFor(rules);
    const parse = function (source) {
        const blockSource = source + "\n\n";
        return rawBuiltParser(blockSource, { inline: false });
    };

    const htmlOutput = SimpleMarkdown.htmlFor(
        SimpleMarkdown.ruleOutput(rules, "html")
    );

    const syntaxTree = parse(value);

    return htmlOutput(syntaxTree);
};

exports.get = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const fileUrl =
        req.query.url ||
        `https://s3.amazonaws.com/${req.params.bucket}/${req.params.path}/${req.params.file}`;

    try {
        const file = await Utils.getBase64ImageS3(fileUrl);
        if (file)
            return res.status(200).json({ fileBase64: file, fileUrl: fileUrl });
    } catch (e) {
        return res.sendStatus(404);
    }
};

exports.convert = (req, res) => {
    const { from, to, value } = req.body;
    if (!from || !to || !value)
        return res
            .status(500)
            .json({ err: "missing from, to or value parameters" });
    if (from === "markdown" && to === "pdf") {
        const markdownRemoved = mdToHtml(value);
        HtmlPdf.create(markdownRemoved, {
            border: {
                top: "20px",
                bottom: "20px",
                left: "20px",
                right: "20px",
            },
            format: "A4",
        }).toBuffer(function (err, buffer) {
            if (err) console.error(err);
            if (Buffer.isBuffer(buffer)) {
                const base64 = buffer.toString("base64");
                const ext = "pdf";
                return res.json({
                    base64,
                    filename: `${+new Date()}.${ext}`,
                });
            }
        });
    } else if (from === "html" && to === "pdf") {
        HtmlPdf.create(value, {
            border: {
                top: "20px",
                bottom: "20px",
                left: "20px",
                right: "20px",
            },
            format: "A4",
        }).toBuffer(function (err, buffer) {
            if (err) console.error(err);
            if (Buffer.isBuffer(buffer)) {
                const base64 = buffer.toString("base64");
                const ext = "pdf";
                return res.json({
                    base64,
                    filename: `${+new Date()}.${ext}`,
                });
            }
        });
    } else {
        return res.json({
            base64: "",
            filename: "",
            err: "Conversion input output unavailable",
        });
    }
};
