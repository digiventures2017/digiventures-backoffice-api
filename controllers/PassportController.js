const config = require("../config/config");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const GoogleStrategy = require("passport-google-oauth2").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;

const AdminUsers = require("../models/adminUsers");
const AuthController = require("./AuthController");
const AuthControllerInstance = new AuthController(AdminUsers);

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(
    new LocalStrategy(
        { passReqToCallback: true },
        async (req, username, password, done) => {
            const result = await AuthControllerInstance.local(
                req,
                username,
                password,
                done
            );
            return done(null, result);
        }
    )
);

passport.use(
    new GoogleStrategy(
        config.auth.google,
        async (req, accessToken, refreshToken, profile, done) => {
            const result = await AuthControllerInstance.google(req, profile);
            return done(null, result);
        }
    )
);

passport.use(
    new FacebookStrategy(
        config.auth.facebook,
        async (req, accessToken, refreshToken, profile, done) => {
            const result = await AuthControllerInstance.facebook(
                req,
                profile,
                done
            );
            return done(null, result);
        }
    )
);
