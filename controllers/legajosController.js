const AdminUsers = require("../models/adminUsers"),
    Handlebars = require("handlebars"),
    HistoryLegajos = require("../models/historyLegajos"),
    HtmlPdf = require("html-pdf"),
    SimpleMarkdown = require("simple-markdown"),
    TenantService = require("../services/TenantService"),
    Utils = require("../config/utils"),
    axios = require("axios"),
    banks = require("../config/banks"),
    countries = require("../config/countries"),
    csvStringify = require("csv-stringify"),
    moment = require("moment-timezone"),
    { checkLegajoComplete } = require("checksteps"),
    { extend, get, set, cloneDeep } = require("lodash"),
    sanitizeHtml = require("sanitize-html"),
    config = require("../config/config"); /* ,
    xlsx = require("js-xlsx"); */

const markdownPaths = ["contract.text", "contract2.text", "privacyPolicy.text"];

const gridsDefault = {
    installments: "Cuotas",
    tna: "TNA",
    tem: "TEM",
    tea: "TEA",
    cft: "CFT",
    amountText: "Capital Total Texto",
    installmentsText: "Cuota Texto",
    installmentsAmount: "Monto por Cuota",
    installmentsAmountText: "Monto por Cuota Texto",
    amountRefunded: "Monto Reintegro",
    expensesForGranting: "Gastos Otorgamiento",
    studyCommission: "Comisión Estudio",
};

const BaseController = require("./Base");
const ScoreService = require("../services/ScoreService");

const ScoreServiceInstance = new ScoreService();
const BaseControllerInstance = new BaseController();

const fixPropertiesWithSpacesObject = (obj) => {
    let str = JSON.stringify(obj);
    const propertiesWithSpace = str.match(/"(\w+[\s]\w+){1,}":/g);
    if (!propertiesWithSpace) return obj;
    propertiesWithSpace.map(
        (p) => (str = str.replace(p, p.replace(/\s/, "_")))
    );
    return JSON.parse(str);
};

const fixPropertiesWithSpaces = (text) => {
    if (!text) return "";
    if (typeof text != "string") return text;
    const matches = text.match(new RegExp("{{(.*?)}}+", "g"));
    if (matches) {
        matches.map((m) => {
            const [, path] = m.match(new RegExp("{{(.*?)}}+"));
            const fixed = path.replace(/\s/, "_");
            text = text.replace(m, `{{${fixed}}}`);
        });
    }
    return text;
};

exports.getAll = async function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const {
        name,
        email,
        phone,
        idNumber,
        datefrom,
        dateto,
        typeScoring,
        status,
        idUser,
        download,
        view,
        limit,
        timezone = "America/Argentina/Buenos_Aires",
        charts,
    } = req.query;

    let {
        offset,
        completeness,
        tenants: selectedTenants,
        page = 0,
        sorting = "",
    } = req.query;

    const owner = req.query.owner ? req.query.owner.split(",") : false;

    if (sorting)
        sorting = sorting.split(",").map((s) => {
            const sv = s.split(":");
            return { [sv[0]]: sv[1] };
        });

    const perPage = view == "funnel" ? 100 : 20;

    if (page >= 1) {
        page--;
    }

    if (!offset) offset = page * perPage;
    const query = {};
    let stringQuery = "";

    query.dev = req.user.test;

    let tenant = [];

    if (!selectedTenants) {
        if (charts) {
            selectedTenants = req.user.tenants[0];
        } else if (req.user.tenants.length === 1) {
            selectedTenants = req.user.tenants[0];
        }
    }

    const tmpSelectedTenants = selectedTenants
        ? selectedTenants.split(",")
        : [];

    if (tmpSelectedTenants.length === 1) {
        tenant = await TenantService.getById(
            tmpSelectedTenants[0],
            req.user.test
        );
    }

    if (name) {
        query.name = name;
        stringQuery += "name=" + name + "&";
    }

    if (phone) {
        query.phone = phone;
        stringQuery += "phone=" + phone + "&";
    }

    if (
        req.user.permissions &&
        req.user.permissions.includes("view-only-assignations")
    ) {
        query["managment.owner._id"] = { $in: [req.user._id] };
    } else if (owner) {
        query["managment.owner._id"] = { $in: owner };
    }

    let completenessHaveNull = false;
    if (completeness && completeness.indexOf("null") > -1) {
        completeness = completeness.replace("null", "");
        completenessHaveNull = true;
    }

    if (typeScoring) {
        const tmpTypeScoring = typeScoring.split(",");
        query.typeScoring = { $in: tmpTypeScoring };
    }

    if (status) {
        const tmpStatus = status.split(",");

        stringQuery += "status=" + tmpStatus + "&";

        query.status = { $in: tmpStatus };
    }

    if (selectedTenants) {
        const tmpSelectedTenants = selectedTenants.split(",");

        stringQuery += "tenants=" + selectedTenants + "&";

        query.tenantId = { $in: tmpSelectedTenants };
    }

    if (completeness || completenessHaveNull) {
        let tmpCompleteness = [];
        tmpCompleteness = completeness.split(",");
        if (completenessHaveNull) {
            tmpCompleteness.push(null);
            completeness = tmpCompleteness.join(",");
        }
        stringQuery += "completeness=" + completeness + "&";
        query.completenessLegajo = { $in: tmpCompleteness };
    }

    if (idUser) {
        query.userId = { $in: [idUser] };
        stringQuery += "idUser=" + idUser + "&";
    }

    if (email) {
        query.email = email;
        stringQuery += "email=" + email + "&";
    }

    if (idNumber) {
        const _idNumber = `${idNumber}`;
        query.idNumber = { $in: `${_idNumber}`.split(" ") };
        stringQuery += "idNumber=" + _idNumber + "&";
    }

    if (datefrom) {
        const date = moment(parseInt(datefrom)).tz(timezone).utc().toDate();

        if (!query.createdAt) {
            query.createdAt = {};
        }

        query.createdAt["$gte"] = date;

        stringQuery += "datefrom=" + datefrom + "&";
    }

    if (dateto) {
        const date = moment(parseInt(dateto))
            .tz(timezone)
            .set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 59,
            })
            .utc()
            .toDate();

        if (!query.createdAt) {
            query.createdAt = {};
        }

        query.createdAt["$lte"] = date;

        stringQuery += "dateto=" + dateto + "&";
    }

    if (req.user.tenants.length) {
        if (!query.tenantId) {
            query.tenantId = { $in: req.user.tenants };
        }
    }

    if (view) {
        stringQuery += "&view=" + view;
        query.view = view;
    }

    let selectFields = {};
    if (view == "funnel") {
        selectFields = {
            _id: 1,
            name: 1,
            statusScoring: 1,
            totalComplete: 1,
            typeScoring: 1,
            status: 1,
            tenantId: 1,
            updatedAt: 1,
            createdAt: 1,
            "managment.owner": 1,
            dev: 1,
            hidden: 1,
            lastActivityAt: 1,
        };
    } else {
        selectFields = {
            _id: 1,
            name: 1,
            email: 1,
            idNumber: 1,
            statusScoring: 1,
            totalComplete: 1,
            status: 1,
            tenantId: 1,
            mailSent: 1,
            updatedAt: 1,
            createdAt: 1,
            mobilePhone: 1,
            activity: 1,
            "managment.owner": 1,
            typeScoring: 1,
            dev: 1,
            completenessLegajo: 1,
            hidden: 1,
            lastActivityAt: 1,
        };
    }

    if (req.user.role != "SuperAdministrador") {
        query.hidden = false;
    }

    let getLegajos = [];

    if (!download) {
        getLegajos = Utils.getLegajos(
            query,
            selectFields,
            limit || perPage,
            offset,
            false,
            false,
            sorting
        );
    } else {
        req.setTimeout(0);
    }

    const tenantResult = TenantService.getAll(req.user.tenants, req.user.test);

    const usersTenants = AdminUsers.find(
        {
            tenants: query.tenantId,
            test: req.user.test,
        },
        "tenants name"
    );

    Promise.all([getLegajos, tenantResult, usersTenants]).then(
        async (values) => {
            const legajos = values[0] || [];
            const tenants = values[1] || [];
            const usersTenants = values[2] || [];

            /*  if (legajos.results && !Array.isArray(legajos.results)) {
                for (const managmentStatus in legajos.results) {
                    if (
                        Object.prototype.hasOwnProperty.call(
                            legajos.results,
                            managmentStatus
                        ) &&
                        legajos.results[managmentStatus].hits
                    ) {
                        const element = legajos.results[managmentStatus].hits;
                        const elements = await setStructures(element);
                        legajos.results[
                            managmentStatus
                        ].hits = await Promise.all(elements);
                    }
                }
            } else if (legajos.results && legajos.results.length) {
                const elements = await setStructures(legajos.results);
                legajos.results = await Promise.all(elements);
            } */

            if (download) {
                exportLegajos(req, res, query, tenants, selectedTenants);
            } else {
                query.sorting = req.query.sorting || "";
                const result = {
                    results: legajos.results,
                    length: legajos.results.length,
                    /* count: legajos.count, */
                    /* totalCountByStatus: legajos.totalCountByStatus, */
                    page: page,
                    perPage: perPage,
                    stringQuery: stringQuery,
                    query: query,
                    tenants: tenants,
                    tenant: tenant,
                    roleUser: req.user.role,
                    idUser: req.user._id,
                    permissions: req.user.permissions || [],
                    usersTenants: usersTenants,
                };
                res.set("MS", Date.now() - req.start);
                res.json(result);
            }
        }
    );
};

exports.getLegajoById = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const { no_tenant, no_navigation, no_history, no_user } = req.headers;

    const legajoId = req.params.id;

    // const urlUsers = Utils.getUrlUsers(req.user.test);

    const legajo = await Utils.getLegajo(legajoId, req.user.test);

    if (
        req.user.permissions &&
        req.user.permissions.includes("view-only-assignations") &&
        get(legajo, "managment.owner._id") != req.user._id
    ) {
        return res.sendStatus(404);
    }

    const getNavigation = no_navigation
        ? null
        : Utils.getNavigation(legajoId, req.user.test);

    const historyResult = no_history
        ? null
        : HistoryLegajos.find({ legajoId: req.params.id })
              .select(
                  "nameField userName valueField oldValueField totalCompleteOld totalCompleteNew created_at"
              )
              .exec();

    Promise.all([historyResult, getNavigation]).then((values) => {
        const getUser = no_user
            ? null
            : Utils.getUser(legajo.userId, req.user.test);

        const historyLegajo = values[0];
        legajo.historyLegajo = historyLegajo;

        const navigationLegajo = values[1];
        legajo.navigationLegajo = navigationLegajo;

        const tenantId = legajo.tenantId;
        const tenantResult = no_tenant
            ? null
            : TenantService.getById(tenantId, req.user.test);

        Promise.all([tenantResult, getUser]).then(async (valuesTenant) => {
            const tenant = valuesTenant[0];
            const user = valuesTenant[1];

            let legajoCheckList = {
                items: [],
                totalComplete: 0,
                checkListImages: {},
            };

            if (legajo.typeScoring && tenant) {
                legajoCheckList = await checkLegajoComplete(tenant, legajo);
            }

            const bankLegajo = get(user, "bank.bank");

            if (bankLegajo) {
                legajo.nameBank = banks[bankLegajo];
            }

            if (legajo.nationality) {
                legajo.nationality = countries[legajo.nationality];
            }

            legajo.completenessLegajoText =
                Utils.getCompletenessLegajoKeyString(legajo.completenessLegajo);

            legajo.whatsapp = "";
            legajo.checkList = legajoCheckList.items;
            legajo.checkListImages = legajoCheckList.checkListImages;
            legajo.user = user;
            legajo.tenant = tenant;
            res.set("MS", Date.now() - req.start);
            res.json(legajo);
        });
    });
};

const saveLegajoHistory = async function (req, res) {
    if (!req.user) {
        return res
            ? res.status(401).json({
                  message: "Unauthorized",
              })
            : false;
    }
    const { id } = req.params;
    const { fields, filesToUpload } = req.body;

    const arInsertHistory = [];
    let bodyUpdateLegajo = {};
    const bodyUpdateUser = {};

    let oldLegajo, tenant;
    try {
        oldLegajo = req.legajo
            ? req.legajo
            : await Utils.getLegajo(id, req.user.dev);
        tenant = req.tenant
            ? req.tenant
            : await TenantService.getById(oldLegajo.tenantId, req.user.test);
    } catch (error) {
        console.error(error);
    }

    const { userId } = oldLegajo;

    setApplicant(oldLegajo, tenant, cloneDeep(req));

    fields.forEach((field) => {
        if (field.name.startsWith("simulation")) {
            const labels = get(
                oldLegajo,
                "configuration.grid.labels",
                gridsDefault
            );
            arInsertHistory.push({
                legajoId: id,
                userId: req.user._id,
                userName: req.user.name,
                nameField:
                    labels[field.name.replace("simulation.", "")] || field.name,
                valueField: field.value,
                oldValueField: field.oldValue,
                totalCompleteOld: oldLegajo.totalComplete,
            });
        } else if (field.name === "notes") {
            field.value.forEach((note, idx) => {
                let valueField, oldValueField;
                // CASE: New note
                if (note.new && !note.removed) {
                    // If the note is new, get the title/content/attached value
                    // nullish coalescing allows empty strings as values, otherwise fallback to edited values (previous)
                    valueField = JSON.stringify({
                        title: note.title ?? undefined,
                        content: note.content ?? undefined,
                        attached: note.attached ?? undefined,
                    });
                    oldValueField = undefined;
                }
                // CASE: Removed note
                else if (!note.new && note.removed) {
                    valueField = undefined;
                    oldValueField = JSON.stringify({
                        title: note.titleEdited ?? undefined,
                        content: note.contentEdited ?? undefined,
                        attached: note.attachedEdited ?? undefined,
                    });
                }
                // CASE: Edited note
                else if (!note.new && !note.removed && note.edited) {
                    valueField = JSON.stringify({
                        title: note.title ?? undefined,
                        content: note.content ?? undefined,
                        attached: note.attached ?? undefined,
                    });
                    oldValueField = JSON.stringify({
                        title: note.titleEdited ?? undefined,
                        content: note.contentEdited ?? undefined,
                        attached: note.attachedEdited ?? undefined,
                    });
                }
                // We only push to history notes that are new, have been removed or are edited
                if (note.edited || note.removed || note.new) {
                    arInsertHistory.push({
                        legajoId: id,
                        userId: req.user._id,
                        userName: req.user.name,
                        nameField: field.name,
                        valueField: valueField,
                        oldValueField: oldValueField,
                        totalCompleteOld: oldLegajo.totalComplete,
                        path:
                            // this helps to match this note later on once the image has been uploaded to S3
                            // so we don't save the history log with the image in base64 but instead using the URL
                            note.new || note.edited
                                ? `notes.${idx}.attached`
                                : undefined,
                    });
                }
            });
        } else {
            arInsertHistory.push({
                legajoId: id,
                userId: req.user._id,
                userName: req.user.name,
                nameField: field.name,
                valueField: field.value,
                oldValueField: field.oldValue,
                totalCompleteOld: oldLegajo.totalComplete,
            });
        }

        if (field.name.indexOf("user.") === 0) {
            bodyUpdateUser[field.name.substr(5)] = field.value;
        } else {
            if (
                field.name.includes("customInput") &&
                typeof field.value === "string"
            ) {
                if (field.value.includes("|")) {
                    const [code, text] = field.value.split("|");
                    let name = field.name.split(".");
                    name.pop();
                    name = name.join(".");
                    bodyUpdateLegajo[`${name}.code`] = code;
                    bodyUpdateLegajo[`${name}.text`] = text;
                }
            }
            bodyUpdateLegajo[field.name] = field.value;
        }
        if (field.name === "status") {
            bodyUpdateLegajo.dateUpdatedStatus = moment();
        }
    });

    const imagesFiles = [];

    bodyUpdateLegajo.notes?.forEach((note, index) => {
        if (note.attached?.includes("data:")) {
            imagesFiles.push(
                Utils.putBase64ImageS3(`notes.${index}.attached`, note.attached)
            );
        }
    });
    // If a note has removed = true, it is filtered out in the legajo (deletes it from the final/updated notes array)
    bodyUpdateLegajo.notes = bodyUpdateLegajo.notes?.filter(
        (note) => !note.removed
    );

    // Information Screen File attachments
    if (filesToUpload && Object.keys(filesToUpload)?.length) {
        Object.entries(filesToUpload).forEach(([key, fileArr]) => {
            fileArr.forEach((file) => {
                if (file.includes("data:")) {
                    imagesFiles.push(Utils.putBase64ImageS3(key, file));
                }
            });
        });
    }

    if (req.body.legajoImages) {
        Object.keys(req.body.legajoImages).forEach((key) => {
            if (req.body.legajoImages[key].includes("data:")) {
                imagesFiles.push(
                    Utils.putBase64ImageS3(
                        `${key}.url`,
                        req.body.legajoImages[key],
                        "digiventures",
                        ""
                    )
                );
                bodyUpdateLegajo[`${key}.manual`] = true;
                bodyUpdateLegajo[`${key}.date`] = Date.now();
                if (key === "idCardFront") {
                    bodyUpdateLegajo[`${key}.dniRekognition`] =
                        String(oldLegajo.idNumber) || "0";
                }
            }
        });
    }
    try {
        const uploadedImages = await Promise.all(imagesFiles);

        const updatedKeyImagesToHistory = [
            "idCardBack",
            "idCardFront",
            "photo",
            "photoValidation",
            "payslip",
            "cbu",
            "utility",
            "signature",
            "notes",
            "information",
        ];
        if (uploadedImages.length) {
            uploadedImages.forEach((image) => {
                if (image.key.includes("notes")) {
                    bodyUpdateLegajo = set(
                        bodyUpdateLegajo,
                        image.key,
                        image.url
                    );
                } else if (image.key.startsWith("information")) {
                    // We add it both to oldLegajo and bodyUpdateLegajo in case there's
                    // a delete operation for the same path later on, to keep all instances updated
                    const answerArr = get(oldLegajo, `${image.key}.answer`, []);

                    answerArr.push(image.url);
                    set(oldLegajo, `${image.key}.answer`, answerArr);
                    bodyUpdateLegajo[`${image.key}.answer`] = get(
                        oldLegajo,
                        `${image.key}.answer`,
                        []
                    );
                } else {
                    bodyUpdateLegajo[image.key] = image.url;
                }

                const imgKey = image.key.split(".");
                const nameField = image.key.includes("video")
                    ? image.key
                    : imgKey[0];
                const oldValueField = get(oldLegajo, nameField);

                if (
                    updatedKeyImagesToHistory.includes(nameField) ||
                    nameField.includes("video.")
                ) {
                    // if it is a note we edit the previously created log instead of pushing a new one
                    const isNote = image.key.startsWith("notes");
                    const isInformation = image.key.startsWith("information");
                    if (isNote) {
                        arInsertHistory.forEach((el) => {
                            if (el.path && el.path === image.key) {
                                el.valueField = JSON.parse(el.valueField);
                                el.valueField.attached = image.url || "";
                                el.valueField = JSON.stringify(el.valueField);
                            }
                        });
                    } else if (isInformation) {
                        const { typeScoring } = oldLegajo;

                        /* This block is used to get the question from the step configuration
                        On an input without any previously uploaded files, only the attachments
                        would be set to the legajo, and not the question, so we need to fetch it
                        from the tenant config */
                        const stepInputs = get(
                            tenant,
                            `steps.${imgKey[0]}/${imgKey[1]}.${typeScoring}.options`
                        );
                        const currentInput = stepInputs.find(
                            (el) => el.reference === imgKey[2]
                        );
                        const { question } = currentInput;
                        bodyUpdateLegajo[`${image.key}.question`] = question;

                        arInsertHistory.push({
                            legajoId: id,
                            userId: req.user._id,
                            userName: req.user.name,
                            nameField: image.key,
                            valueField: image.url || "",
                            oldValueField: "",
                            totalCompleteOld: oldLegajo.totalComplete,
                        });
                    } else {
                        arInsertHistory.push({
                            legajoId: id,
                            userId: req.user._id,
                            userName: req.user.name,
                            nameField: nameField,
                            valueField: image.url || "",
                            oldValueField: oldValueField
                                ? JSON.stringify(oldValueField)
                                : "",
                            totalCompleteOld: oldLegajo.totalComplete,
                        });
                    }
                }
            });
        }
        if (req.body.$unset) {
            // checks if the provided key cointains bracket array notation (something[0].something[2])
            const isArrNotation = (key) => key.match(/(\[)(\d)(\])/g);
            // Replaces bracket notation for arrays ("something[0]") for dot notation ("something.0")
            const arrToDotNotation = (key) =>
                key.replace(/(\[)(\d)(\])/g, ".$2");
            // checks if the key ends with an array index (something[0].something doesn't match, something[0] does)
            const endsInArr = (key) => key.match(/(\[)(\d)(\])$/g);

            bodyUpdateLegajo.$unset = req.body.$unset;
            const pathsToFilter = [];

            if (Array.isArray(bodyUpdateLegajo.$unset)) {
                bodyUpdateLegajo.$unset = bodyUpdateLegajo.$unset.filter(
                    (unsetKey) => {
                        const oldValueField = get(oldLegajo, unsetKey);
                        // removes array notation from nameField so normalization on the backoffice frontend
                        // can be done easier without having to consider each possible position on the array
                        const filteredNameField = unsetKey.replace(
                            /.answer(\[)(\d)(\])$/g,
                            ""
                        );
                        arInsertHistory.push({
                            legajoId: id,
                            userId: req.user._id,
                            userName: req.user.name,
                            nameField: filteredNameField,
                            valueField: "",
                            oldValueField:
                                // Stringification kept for compatibility, to be removed in the future
                                typeof oldValueField === "string"
                                    ? oldValueField
                                    : JSON.stringify(oldValueField || null),
                            totalCompleteOld: oldLegajo.totalComplete,
                        });

                        const isArrKey = !!isArrNotation(unsetKey);
                        if (isArrKey) {
                            // arrays need to be set in dot notation
                            const replacedKey = arrToDotNotation(unsetKey);
                            // We obtain the whole object, set the index be unset to null and later
                            // filter the nulls and re-set the whole object again. This is because
                            // schema is types.Mixed in mongo -> it doesn't merge new data with previous values
                            if (endsInArr(unsetKey)) {
                                const path = replacedKey
                                    .split(".")
                                    .slice(0, -1)
                                    .join(".");
                                set(oldLegajo, unsetKey, null);
                                // Once set, we filter the $unset rule to prevent it reaching mongo
                                if (!pathsToFilter.includes(path))
                                    pathsToFilter.push(path);
                            }
                            return false;
                        }
                        return true;
                    }
                );
            }
            // The whole object is updated and added to bodyUpdateLegajo to be saved later on
            pathsToFilter.forEach((path) => {
                const answer = get(oldLegajo, path, []);
                const filtered = answer.filter((answ) => answ);
                bodyUpdateLegajo[path] = filtered;
            });
        }

        const saveHistory = HistoryLegajos.insertMany(arInsertHistory);
        const saveLegajo = Utils.updateLegajo(
            id,
            bodyUpdateLegajo,
            req.user.test,
            "id",
            false,
            { notificate: false, index: false }
        );
        const saveUser = Utils.updateUser(
            userId,
            bodyUpdateUser,
            req.user.test
        );
        try {
            const resolvedSaves = await Promise.all([
                saveHistory,
                saveLegajo,
                saveUser,
            ]);
            const [histories, legajoUpdated] = resolvedSaves;
            if (legajoUpdated.err)
                return res
                    ? res.json({
                          error: true,
                          message: "Error al guardar el legajo.",
                      })
                    : false;

            if (tenant) {
                //update legajoComplete on background
                const checkLegajo = checkLegajoComplete(tenant, legajoUpdated);

                const body = {
                    totalComplete: checkLegajo.totalComplete,
                    completenessLegajo: checkLegajo.completenessStatus,
                };

                if (!legajoUpdated.firstUserModifier) {
                    body.firstUserModifier = histories[histories.length - 1];
                    if (body.firstUserModifier)
                        body.firstUserModifier.totalCompleteNew =
                            checkLegajo.totalComplete;
                }

                body.lastUserModifier = histories[histories.length - 1];
                if (body.lastUserModifier) {
                    body.lastUserModifier.totalCompleteNew =
                        checkLegajo?.totalComplete;
                }

                if (
                    legajoUpdated.totalCompleteUserAdmin === null ||
                    isNaN(legajoUpdated.totalCompleteUserAdmin)
                ) {
                    legajoUpdated.totalCompleteUserAdmin = 0;
                }

                const totalCompleteNew =
                    body?.lastUserModifier?.totalCompleteNew || 0;
                const totalCompleteOld =
                    body?.lastUserModifier?.totalCompleteOld || 0;
                body.totalCompleteUserAdmin =
                    legajoUpdated.totalCompleteUserAdmin +
                    (totalCompleteNew - totalCompleteOld);
                Utils.updateLegajo(id, body, req.user.test);

                const idsHistory = histories.map((history) => {
                    return history._id;
                });

                await HistoryLegajos.update(
                    { _id: { $in: idsHistory } },
                    {
                        $set: {
                            totalCompleteNew: checkLegajo.totalComplete,
                        },
                    },
                    { multi: true }
                );
            }
            if (res) {
                res.set("MS", Date.now() - req.start);
                return res.json({
                    message: "Datos guardado correctamente.",
                    data: resolvedSaves,
                });
            }
            return false;
        } catch (error) {
            console.error(error);
            if (res) {
                res.set("MS", Date.now() - req.start);
                return res.json({
                    error: true,
                    message: "Error al guardar el legajo.",
                });
            }
            return false;
        }
    } catch (error) {
        console.error(error);
        if (res) {
            res.set("MS", Date.now() - req.start);
            return res.json({
                error: true,
                message: "Error al guardar el legajo.",
            });
        }
        return false;
    }
};

exports.saveLegajoHistory = saveLegajoHistory;

async function setApplicant(oldLegajo, tenant, req) {
    const applicantChange = req.body.fields.find(
        (field) => field.name === "applicantParent"
    );

    if (applicantChange) {
        const applicantReq = req;
        const legajoApplicant = await Utils.getLegajo(
            applicantChange.value,
            req.user.dev
        );
        const tenantApplicant = await TenantService.getById(
            legajoApplicant.tenantId,
            req.user.test
        );
        applicantReq.legajo = legajoApplicant;
        applicantReq.tenant = tenantApplicant;
        req.params.id = legajoApplicant._id;

        const { links = {} } = tenantApplicant;
        let link = {};
        let property = "";

        Object.entries(links).map(([key, value]) => {
            if (value.domain.includes(tenant.subdomain)) {
                link = value;
                property = key;
            }
        });

        if (!property) return;

        const applicantIds = get(legajoApplicant, `links.${link}._ids`, []);

        if (!applicantIds.includes(oldLegajo._id)) {
            applicantIds.push(oldLegajo._id);

            applicantReq.body = {
                fields: [
                    {
                        name: `linked.${property}._ids`,
                        value: applicantIds,
                    },
                ],
            };

            saveLegajoHistory(applicantReq);
        }
    }
}

async function exportLegajos(
    req,
    res,
    query,
    tenants,
    tenantId,
    offset = 0,
    dataRow = []
) {
    const tenant = await TenantService.getById(tenantId, req.user.test);

    const normalizationConstants = get(tenant, "constants.normalization.arr");

    const columns = {
        _id: "ID",
        name: "Nombre",
        email: "E-Mail",
        idNumber: "DNI",
        legajoComplete: "Estado de Legajo",
        tenantId: "Tenant",
        mailSent: "Mail Enviado",
        createdAt: "Fecha",
    };

    const selectFields = {
        _id: 1,
        name: 1,
        email: 1,
        idNumber: 1,
        legajoComplete: 1,
        tenantId: 1,
        mailSent: 1,
        createdAt: 1,
    };

    const nameFieldsDate = [
        "paymentDate",
        "disbursementDate",
        "photo.renaperTimestamp",
        "signature.date",
        "contract.date",
        "contract2.date",
        "dateUpdatedStatus",
        "createdAt",
        "updatedAt",
    ];

    let columnsList = [];
    let newSelectFields = {};
    const rowsList = [];

    if (normalizationConstants) {
        normalizationConstants.forEach((field) => {
            let nameField = field.field;
            let selectField = field.field;
            if (selectField.startsWith("simulation")) {
                selectField = "simulation";
            }
            if (nameField.includes("|")) {
                const splitField = nameField.split("|");
                nameField = splitField[0];
                selectField = splitField[1];
            }

            if (field.exportCsv) {
                newSelectFields[selectField] = 1;
                columnsList.push(field.name);
                rowsList.push(nameField);
            }
        });
    }

    if (!columnsList.length) {
        columnsList = columns;
        newSelectFields = selectFields;
    }

    const limitLegajos = 2000;

    const legajos = await Utils.getLegajos(
        query,
        newSelectFields,
        limitLegajos,
        offset,
        true
    );

    const linked = {};
    /* const { links } = tenant; */

    legajos.results.forEach((legajo) => {
        const tmpRow = [];
        for (const i in rowsList) {
            const nameField = rowsList[i];
            let value = get(legajo, nameField);

            if (nameField == "tenantId") {
                if (tenants[value]) {
                    value = tenants[value].name;
                } else {
                    value = "";
                }
            } else if (nameField == "legajoComplete") {
                value = config.legajoStatusText[value];
            } else if (nameField == "mailSent" && value) {
                value = "Enviado";
            } else if (nameFieldsDate.includes(nameField)) {
                if (value) {
                    value = moment(value)
                        .tz("America/Argentina/Buenos_Aires")
                        .format("DD/MM/YYYY HH:mm");
                }
            } else if (nameField === "linked") {
                if (value) {
                    Object.entries(value).map(([reference, value]) => {
                        if (!linked[reference])
                            linked[reference] = { dependencies: [] };
                        linked[reference].dependencies[legajo._id] =
                            value._ids || [];
                    });
                }
                value = "";
            }
            tmpRow.push(value);
        }

        dataRow.push(tmpRow);
    });

    if (legajos.results.length >= limitLegajos) {
        exportLegajos(
            req,
            res,
            query,
            tenants,
            tenantId,
            offset + limitLegajos,
            dataRow
        );
    } else {
        csvStringify(
            dataRow,
            { header: true, columns: columnsList, delimiter: ";" },
            (err, output) => {
                res.set("Content-Type", "text/csv; charset=utf-8");
                res.set(
                    "Content-Disposition",
                    "inline; filename=legajos-" +
                        moment()
                            .tz("America/Argentina/Buenos_Aires")
                            .format("DD-MM-YYYY-HH-mm") +
                        ".csv"
                );
                res.write(output);
                res.end();
            }
        );
    }
}

exports.getLegajoPdfById = async function (req, res) {
    const queryTest = get(req, "query.test");

    if (!queryTest) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    let newDev = false;

    if (queryTest) {
        newDev = queryTest === "true";
    }

    const legajoId = req.params.id;

    const arFieldsDate = [
        "paymentDate",
        "disbursementDate",
        "photo.renaperTimestamp",
        "signature.date",
        "contract.date",
        "contract2.date",
        "dateUpdatedStatus",
        "createdAt",
        "updatedAt",
    ];

    const contractProps = ["contract.text", "contract2.text"];

    const legajo = await Utils.getLegajo(
        legajoId,
        req.user ? req.user.test : newDev
    );

    const getUser = Utils.getUser(
        legajo.userId,
        req.user ? req.user.test : newDev
    );

    const tenantId = legajo.tenantId;

    const tenantResult = TenantService.getById(
        tenantId,
        req.user ? req.user.test : newDev
    );

    Promise.all([tenantResult, getUser]).then((valuesTenant) => {
        const tenant = valuesTenant[0];
        const user = valuesTenant[1];

        let dataPdf = get(tenant, `constants.pdf.${legajo.typeScoring}`);
        if (!legajo.typeScoring) {
            dataPdf = get(tenant, `constants.pdf.content`);
        }

        if (!dataPdf) {
            /* res.set("MS", Date.now() - req.start); */
            return res.json({});
        }

        const firstVariable = Utils.getIndicesOf("{{", dataPdf),
            secondVariable = Utils.getIndicesOf("}}", dataPdf);

        let newDataPdf = dataPdf;
        const promiseImages = [];
        const namesImages = [];

        firstVariable.forEach(function (firstIndex, index) {
            const field = dataPdf.substr(
                firstIndex + 2,
                secondVariable[index] - firstIndex - 2
            );
            const splitField = field.split(".");

            if (splitField[0] == "legajo") {
                splitField.shift();

                let nameField = splitField[0];

                if (splitField.length > 1) {
                    nameField = splitField.join(".");
                }

                const urlSplit = nameField.split("|");
                let possibleUrlImages = get(legajo, urlSplit[0], "");
                if (
                    String(possibleUrlImages).startsWith(
                        "https://s3.amazonaws.com"
                    )
                ) {
                    if (typeof possibleUrlImages == "string") {
                        possibleUrlImages = [possibleUrlImages];
                    }
                    possibleUrlImages.map((urlImage) => {
                        if (
                            urlImage &&
                            !urlImage.includes("data:application/octet-stream")
                        ) {
                            promiseImages.push(
                                Utils.getBase64ImageS3(urlImage)
                            );
                            const objImage = { field };

                            if (urlSplit[1]) {
                                const splitSize = urlSplit[1].split(":");

                                objImage[splitSize[0]] = splitSize[1];
                            }

                            if (urlSplit[2]) {
                                const splitSize = urlSplit[2].split(":");

                                objImage[splitSize[0]] = splitSize[1];
                            }

                            namesImages.push(objImage);
                        } else {
                            newDataPdf = newDataPdf.replace(`{{${field}}}`, "");
                        }
                    });
                } else {
                    let value = get(legajo, nameField);

                    if (arFieldsDate.indexOf(nameField) > -1) {
                        value = moment(value)
                            .tz("America/Argentina/Buenos_Aires")
                            .format("HH:mm DD-MM-YYYY");
                    }

                    if (contractProps.indexOf(nameField) > -1) {
                        value = mdToHtml(value);
                    }

                    if (value) {
                        if (typeof value == "string")
                            value = value.replace(/(&nbsp;|&nbsp|nbsp)/g, "");
                        newDataPdf = newDataPdf.replace(
                            "{{" + field + "}}",
                            value
                        );
                    } else {
                        newDataPdf = newDataPdf.replace(
                            "{{" + field + "}}",
                            ""
                        );
                    }
                }
            } else if (splitField[0] == "user") {
                splitField.shift();

                let nameField = splitField[0];

                if (splitField.length > 1) {
                    nameField = splitField.join(".");
                }

                let value = get(user, nameField);

                if (arFieldsDate.indexOf(nameField) > -1) {
                    value = moment(value)
                        .tz("America/Argentina/Buenos_Aires")
                        .format("HH:mm DD-MM-YYYY");
                }

                if (value) {
                    newDataPdf = newDataPdf.replace("{{" + field + "}}", value);
                } else {
                    newDataPdf = newDataPdf.replace("{{" + field + "}}", "");
                }
            }
        });

        Promise.all(promiseImages).then((values) => {
            namesImages.forEach((value, index) => {
                const base64 = values[index];

                let tagImg = '<img src="' + base64 + '" ';

                if (value.width) {
                    tagImg += 'width="' + value.width + '" ';
                }

                if (value.height) {
                    tagImg += 'height="' + value.height + '" ';
                }

                tagImg += " />";

                newDataPdf = newDataPdf.replace(
                    "{{" + value.field + "}}",
                    tagImg
                );
            });

            HtmlPdf.create(newDataPdf, {
                border: {
                    top: "20px",
                    bottom: "20px",
                    left: "20px",
                    right: "20px",
                },
                format: "A4",
            }).toBuffer(function (err, buffer) {
                if (Buffer.isBuffer(buffer)) {
                    res.set("Content-Type", "application/pdf; charset=utf-8");
                    res.set(
                        "Content-Disposition",
                        "attachment; filename=legajo-" +
                            moment()
                                .tz("America/Argentina/Buenos_Aires")
                                .format("DD-MM-YYYY-HH-mm") +
                            ".pdf"
                    );
                    res.write(buffer);
                    return res.end();
                } else {
                    return res.json({});
                }
            });
        });
    });
};

exports.getLegajoPdfById2 = async function (req, res) {
    const queryTest = get(req, "query.test");

    if (!queryTest) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    let newDev = false;

    if (queryTest) {
        newDev = queryTest === "true";
    }

    const legajoId = req.params.id;

    let legajo = await Utils.getLegajo(
        legajoId,
        req.user ? req.user.test : newDev
    );

    if (legajo.video) delete legajo.video;
    legajo = setDatesFormats(legajo);
    legajo = setMarkDownToHTML(legajo);

    const tenantId = legajo.tenantId;

    const tenant = await TenantService.getById(
        tenantId,
        req.user ? req.user.test : newDev
    );

    let dataPdf = get(tenant, `constants.pdf.${legajo.typeScoring}`);
    if (!legajo.typeScoring) {
        dataPdf = get(tenant, `constants.pdf.content`);
    }

    if (!dataPdf) {
        /* res.set("MS", Date.now() - req.start); */
        return res.json({});
    }

    dataPdf = sanitizeHtml(dataPdf, config.html);

    markdownPaths.map((markdownPath) => {
        const brk1 = [
            `{{${markdownPath}}}`,
            `{{this.${markdownPath}}}`,
            `{{legajo.${markdownPath}}}`,
        ];
        brk1.map((brk) => {
            const brk2 = `{${brk}}`;
            dataPdf = dataPdf.replace(new RegExp(brk, "g"), brk2);
        });
    });

    const getFile = async (fileUrl, path) => {
        try {
            const file = await Utils.getBase64ImageS3(fileUrl);
            return { file, path };
        } catch (e) {
            return { file: "", path };
        }
    };

    const filesPromises = [];

    const generate = (data, text) => {
        if (!text) return "";
        if (typeof text != "string") return text;
        const matches = text.match(new RegExp("{{(.*?)}}+", "g"));
        text = text.replace(/\|/g, "__");
        if (matches) {
            matches.map((m) => {
                let value = "";
                if (m.includes(".url|")) {
                    const cleaned = m.replace(/{|}/g, "");
                    const [urlProperty, width] = cleaned.split("|");
                    const [, widthSize] = (width || "").split(":");
                    const widthStr =
                        width && widthSize ? `width="${widthSize}"` : "";
                    value = `<img src="{{${urlProperty}}}" ${widthStr} />`;
                    const fileUrl = get(data, urlProperty);
                    if (fileUrl)
                        filesPromises.push(getFile(fileUrl, urlProperty));
                }
                m = m.replace(/\|/g, "__");
                if (value) text = text.replace(new RegExp(m, "g"), value);
            });
        }
        return text;
    };

    const seed = {
        legajo,
        tenant,
    };

    let templateGeneration = generate(seed, dataPdf);
    const files = await Promise.all(filesPromises);

    files.map((file) => {
        set(seed, file.path, file.file);
    });

    const { linked, dev } = legajo;
    const linkedPromise = [];

    const getLinked = async (_id, dev, reference) => {
        let linkedLegajo = await Utils.getLegajo(_id, dev);
        if (linkedLegajo.video) delete linkedLegajo.video;
        linkedLegajo = setDatesFormats(linkedLegajo);
        linkedLegajo = setMarkDownToHTML(linkedLegajo);
        return {
            legajo: linkedLegajo,
            path: `legajo.linked.${reference}.legajos`,
        };
    };

    Object.entries(linked || {}).map(([reference, link]) =>
        link._ids
            ? link._ids.map((_id) =>
                  linkedPromise.push(getLinked(_id, dev, reference))
              )
            : null
    );

    const linkedLegajos = await Promise.all(linkedPromise);
    linkedLegajos.map((linkedLegajo) => {
        const value = get(seed, linkedLegajo.path, []);
        value.push(linkedLegajo.legajo);
        set(seed, linkedLegajo.path, value);
    });

    const NormalizerRegex = /{{(.*?)\[(.*?)\](.*?)}}/g;
    const detections = templateGeneration.match(NormalizerRegex);
    if (detections) {
        detections.map((detection) => {
            const fixed = detection
                .replace(/\[/g, ".")
                .replace(/\]\./g, ".")
                .replace(/\]/g, "");
            templateGeneration = templateGeneration.replace(detection, fixed);
        });
    }

    const template = Handlebars.compile(templateGeneration);
    let output = template(seed);

    const outputFilesPending = [];
    const outputFilesMatch = output.match(/(src=")([^"]+)(")/g);
    if (outputFilesMatch) {
        outputFilesMatch.map((file) => {
            const [, , url] = file.match(/(src=")([^"]+)(")/);
            if (url.startsWith("http"))
                outputFilesPending.push(getFile(url, url));
        });
    }
    const outputFiles = await Promise.all(outputFilesPending);

    outputFiles.map(({ file, path }) => {
        output = output.replace(path, file);
    });

    HtmlPdf.create(output, {
        border: {
            top: "20px",
            bottom: "20px",
            left: "20px",
            right: "20px",
        },
        format: "A4",
    }).toBuffer((err, buffer) => {
        if (Buffer.isBuffer(buffer)) {
            res.set("Content-Type", "application/pdf; charset=utf-8");
            res.set(
                "Content-Disposition",
                "attachment; filename=legajo-" +
                    moment()
                        .tz("America/Argentina/Buenos_Aires")
                        .format("DD-MM-YYYY-HH-mm") +
                    ".pdf"
            );
            res.write(buffer);
            return res.end();
        } else {
            return res.json({});
        }
    });
};

function setMarkDownToHTML(obj) {
    markdownPaths.map((path) => {
        const md = get(obj, path);
        if (md) {
            const html = mdToHtml(md);
            obj = set(obj, path, html);
        }
    });

    return obj;
}

function setDatesFormats(obj) {
    let str = JSON.stringify(obj);

    const regexMultipleResults =
        /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+).(\d+)(\w)/g;

    const regexSingularAgrupations =
        /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+).(\d+)(\w)/;

    const detections = str.match(regexMultipleResults);

    if (detections) {
        detections.map((detection) => {
            const [, year, month, day, hour, minutes, seconds, , ,] =
                detection.match(regexSingularAgrupations);
            str = str.replace(
                detection,
                `${day}/${month}/${year} ${hour}:${minutes}:${seconds}`
            );
        });
    }

    return JSON.parse(str);
}

/*
 value = moment(value)
                            .tz("America/Argentina/Buenos_Aires")
                            .format("HH:mm DD-MM-YYYY");
*/

exports.saveAssignedUsers = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const id = req.params.id;
    const action = req.params.action;
    let haveOwner = false;
    let currentOwnerName = "";
    let currentOwnerId = "";
    const body = {};
    let legajo = {};
    const force = req.body.force;

    const ownerNameField = "managment.owner.name";

    legajo = await Utils.getLegajo(id, req.user.test);
    currentOwnerName = get(legajo, ownerNameField);
    currentOwnerId = get(legajo, "managment.owner._id");
    haveOwner = !!currentOwnerName || !!currentOwnerId;

    if (haveOwner && !force && (action == "join" || action == "set")) {
        return res.status(403).json({ name: currentOwnerName });
    } else if (
        haveOwner &&
        action == "leave" &&
        currentOwnerId != req.user._id
    ) {
        return res
            .status(401)
            .json({ _id: currentOwnerId || "", name: currentOwnerName || "" });
    }

    if (action == "join") {
        body[`managment.owner`] = {
            _id: req.user._id,
            name: req.user.name,
            email: req.user.email,
        };
    } else if (action == "leave") {
        body["managment.owner"] = { _id: "", name: "", email: "" };
    } else if (action == "set") {
        body["managment.owner"] = {
            _id: req.body._id,
            name: req.body.name,
            email: req.body.email,
        };
    }

    const legajoUpdated = await Utils.updateLegajo(
        id,
        body,
        req.user.test,
        "id",
        false
    );

    if (legajoUpdated) {
        try {
            const history = new HistoryLegajos({
                legajoId: legajoUpdated._id,
                userId: req.user._id,
                userName: req.user.name,
                nameField: ownerNameField,
                valueField: get(legajoUpdated, ownerNameField),
                oldValueField: currentOwnerName,
                totalCompleteOld: legajo.totalComplete,
                totalCompleteNew: legajo.totalComplete,
            });
            history.save();
        } catch (e) {
            console.error(e);
        }
        res.set("MS", Date.now() - req.start);
        return res.json(legajoUpdated.managment || {});
    }
    return res.sendStatus(500);
};

exports.saveStatus = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    const id = req.params.id;
    const newStatus = req.body.newStatus;
    const oldStatus = req.body.oldStatus;
    const body = {};

    const legajo = await Utils.getLegajo(id, req.user.test);

    if (legajo.status != oldStatus) {
        return res.status(403).json({ currentStatus: legajo.status });
    }
    body.status = newStatus;

    const legajoUpdated = await Utils.updateLegajo(id, body, req.user.test);

    if (legajoUpdated) {
        try {
            const history = new HistoryLegajos({
                legajoId: legajoUpdated._id,
                userId: req.user._id,
                userName: req.user.name,
                nameField: "status",
                valueField: legajoUpdated.status,
                oldValueField: legajo.status,
                totalCompleteOld: legajo.totalComplete,
                totalCompleteNew: legajo.totalComplete,
            });
            history.save();
        } catch (e) {
            console.error(e);
        }
        return res.sendStatus(200);
    }
    return res.sendStatus(500);
};

exports.deleteLegajo = function (req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }

    const idLegajo = req.params.id;

    if (idLegajo) {
        return Utils.deleteLegajo(idLegajo, req.user.test)
            .then(() => {
                res.set("MS", Date.now() - req.start);
                return res.json({
                    deleted: true,
                });
            })
            .catch((err) => {
                console.error("ERR CATCH: " + new Date().toString());
                console.error(err);
                console.error("-------------------------------------------");
            });
    }
};

function mdToHtml(value) {
    if (!value) return "";
    const underlineRule = {
        // Specify the order in which this rule is to be run
        order: SimpleMarkdown.defaultRules.em.order - 0.5,

        // First we check whether a string matches
        match: function (source) {
            return /^\+\+([\s\S]+?)\+\+(?!_)/.exec(source);
        },

        // Then parse this string into a syntax node
        parse: function (capture, parse, state) {
            return {
                content: parse(capture[1], state),
            };
        },

        // Or an html element:
        // (Note: you may only need to make one of `react:` or
        // `html:`, as long as you never ask for an outputter
        // for the other type.)
        html: function (node, output) {
            return "<u>" + output(node.content) + "</u>";
        },
    };

    const rules = extend({}, SimpleMarkdown.defaultRules, {
        underline: underlineRule,
    });

    const rawBuiltParser = SimpleMarkdown.parserFor(rules);
    const parse = function (source) {
        const blockSource = source + "\n\n";
        return rawBuiltParser(blockSource, { inline: false });
    };

    const htmlOutput = SimpleMarkdown.htmlFor(
        SimpleMarkdown.ruleOutput(rules, "html")
    );

    const syntaxTree = parse(value);

    return htmlOutput(syntaxTree);
}

exports.migrate = async (req, res) => {
    if (
        !req.query.code ||
        req.query.code !=
            "81fedfab6b4219af99d6945d1872502ff47c6734531b9696165f1c0cc9576383"
    )
        return res.sendStatus(403);
    const response = await axios.post(
        Utils.getUrlLegajos(req.body.dev) + "/legajos/migrate",
        req.body
    );

    return res.status(response.status).json(response.data);
};

exports.setScore = async (req, res) => {
    const { user: userAuthed } = req;
    if (!userAuthed) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }
    const { _id } = req.params;
    const { configurationId } = req.body;

    if (!configurationId)
        return res
            .status(500)
            .json({ message: "ERROR: configurationId missing" });

    const { tenant, legajo, dev } = await BaseControllerInstance.init(
        _id,
        userAuthed.dev,
        true,
        true
    );

    const score = await ScoreServiceInstance.getScoring(
        tenant,
        legajo,
        {
            dev,
        },
        configurationId
    );

    if (!score) return res.sendStatus(500);

    try {
        const legajoUpdated = await Utils.updateLegajo(_id, score.legajo, dev);

        const checkLegajo = checkLegajoComplete(tenant, legajoUpdated);

        if (
            legajoUpdated.totalComplete != checkLegajo.totalComplete ||
            legajoUpdated.completenessStatus != checkLegajo.completenessStatus
        ) {
            const body = {
                totalComplete: checkLegajo.totalComplete,
                completenessLegajo: checkLegajo.completenessStatus,
            };

            await Utils.updateLegajo(_id, body, req.user.test);
        }

        const historyScoring = new HistoryLegajos({
            legajoId: _id,
            userId: userAuthed._id,
            userName: userAuthed.name,
            nameField: "typeScoring",
            valueField: legajoUpdated.typeScoring,
            oldValueField: legajo.typeScoring,
            totalCompleteOld: legajo.totalComplete,
            totalCompleteNew: legajoUpdated.totalComplete,
        });
        historyScoring.save();

        if (legajo.status != legajoUpdated.status) {
            const historyStatus = new HistoryLegajos({
                legajoId: _id,
                userId: userAuthed._id,
                userName: userAuthed.name,
                nameField: "status",
                valueField: legajoUpdated.status,
                oldValueField: legajo.status,
                totalCompleteOld: legajo.totalComplete,
                totalCompleteNew: legajoUpdated.totalComplete,
            });
            historyStatus.save();
        }

        return res.json(score);
    } catch (e) {
        return res.sendStatus(500);
    }
};

exports.setScores = async (req, res) => {
    const { user: userAuthed } = req;
    if (!userAuthed) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }
    const _ids = req.body;

    for (let index = 0; index < _ids.length; index++) {
        try {
            const _id = _ids[index];
            const { tenant, legajo, user, dev } =
                await BaseControllerInstance.init(
                    _id,
                    userAuthed.dev,
                    true,
                    true
                );

            const score = await ScoreServiceInstance.getScoring(
                tenant,
                user,
                legajo,
                {
                    dev,
                }
            );

            if (!score) {
                continue;
            }

            try {
                const legajoUpdated = await Utils.updateLegajo(
                    _id,
                    score.legajo,
                    dev
                );

                const checkLegajo = checkLegajoComplete(tenant, legajoUpdated);

                if (
                    legajoUpdated.totalComplete != checkLegajo.totalComplete ||
                    legajoUpdated.completenessStatus !=
                        checkLegajo.completenessStatus
                ) {
                    const body = {
                        totalComplete: checkLegajo.totalComplete,
                        completenessLegajo: checkLegajo.completenessStatus,
                    };

                    await Utils.updateLegajo(_id, body, req.user.test);
                }
            } catch (e) {
                console.error("error", e, _id);
            }
        } catch (e) {
            console.error(e);
        }
    }
};

exports.migrateUserLegajo = async (req, res) => {
    const { dev } = req.query;
    let { userId } = req.query;
    let start = true;
    let hasResult = true;
    const ids = [];
    let count = 0;
    do {
        const query = start ? { _id: userId } : { _id: { $gt: userId } };
        const limit = start ? 1 : 100;
        const getUser = await Utils.getUsers(
            query,
            { gender: 1, civilStatus: 1, nationality: 1 },
            limit,
            "",
            dev,
            { _id: 1 }
        );
        start = false;
        if (!getUser.results.length) {
            hasResult = false;
        } else {
            userId = getUser.results[getUser.results.length - 1]._id;
            const promises = getUser.results.map((user) => {
                const { _id, gender, civilStatus, nationality } = user;
                return Utils.updateLegajosByUserId(_id, {
                    gender,
                    civilStatus,
                    nationality,
                });
            });
            count = count + getUser.results.length;
            start = false;
            const updated = await Promise.all(promises);
            ids.push({ batch: count, results: updated });
        }
    } while (hasResult);

    return res.json(ids);
};

/* async function setStructure(legajo) {
    const path = "constants.scoringConfiguration.references";
    const scoringConfigJson = config.scoringConfiguration.references;
    const { typeScoring } = legajo;
    // const time = +new Date();
    let tenant = null;
    let conf = constants.scoringReferences[legajo.tenantId];
    if (!conf)
        try {
            tenant = await TenantService.getById(legajo.tenantId, legajo.dev);
            conf = get(tenant, path, scoringConfigJson);
            if (conf) constants.scoringReferences[legajo.tenantId] = conf;
        } catch (e) {
            return legajo;
        }
    if (typeScoring) {
        try {
            const ctScoring = conf[typeScoring] || {};
            legajo.statusText = ctScoring.label ? ctScoring.label : "";
            legajo.statusType = ctScoring.type ? ctScoring.type : "";
        } catch (e) {
            console.log(e);
        }
    }
    return legajo;
}

async function setStructures(legajos) {
    return legajos.map((legajo) => setStructure(legajo)); 
}*/
