const bigqueryapi = require("bigquery-api");
const utils = require("../config/utils");
const _ = require("lodash");

let tablePrefix = utils.getBigQueryTable();

exports.query = (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }
    let tableName = req.params.table === "none" ? false : req.params.table;
    try {
        bigqueryapi({
            projectId: "digiventures",
            keyFilename: "bigquery-key.json",
            datasetId: tablePrefix,
        }).then(async ({ bigquery, table }) => {
            if (!tableName) {
                let tables = Object.keys(table);
                tableName = tables[tables.length - 1];
            }

            if (!tableName) {
                return res.status(404).json({ error: "table not found" });
            }

            let selectedTenants = req.body.selectedTenants;
            // query to select and check sync
            if (!selectedTenants && req.user.tenants.length) {
                selectedTenants = req.user.tenants;
            }

            let haveTenant = selectedTenants
                ? `tenantId IN ("${selectedTenants.join('", "')}")${
                      req.body.where ? ` AND ` : ""
                  }`
                : "";

            const BQ_Query = {
                query: `SELECT ${
                    req.body.fields ? req.body.fields : "*"
                } FROM ${
                    req.body.from
                        ? req.body.from
                        : `digiventures.${tablePrefix}.${tableName}`
                }${
                    req.body.where || haveTenant
                        ? ` WHERE ${haveTenant ? haveTenant : ""}${
                              req.body.where ? req.body.where : ""
                          }`
                        : ""
                }${req.body.adds ? " " + req.body.adds : ""}`,
            };
            let BQSelectAll = await bigquery.query(BQ_Query);

            BQSelectAll = BQSelectAll[0];

            switch (req.query.chart) {
                case "rejectionMessage":
                    BQSelectAll = rejectionMessagePieChart(BQSelectAll);
                    break;
                case "settledByPeriod":
                    BQSelectAll = settledByPeriod(BQSelectAll);
                    break;
                default:
                    break;
            }

            return res.status(200).json({
                ready: true,
                results: BQSelectAll,
                tenantManager: [req.user.tenants[0]],
            });
        });
    } catch (e) {
        return res.status(500).json({ error: e });
    }
};

function rejectionMessagePieChart(obj) {
    let model = {
        labels: [],
        datasets: [
            {
                data: [],
                backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
                hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
            },
        ],
    };
    obj.map((o) => {
        if (o["rejectionMessage"]) {
            model.labels.push(o["rejectionMessage"]);
            model.datasets[0].data.push(o["f0_"]);
            model.datasets[0].backgroundColor.push(randomHexa());
            model.datasets[0].hoverBackgroundColor.push(randomHexa());
        }
    });
    return model;
}

function settledByPeriod(obj) {
    let model = {
        labels: [],
        datasets: [
            {
                label: "Estados finales",
                backgroundColor: "rgba(255,99,132,0.2)",
                borderColor: "rgba(255,99,132,1)",
                borderWidth: 1,
                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                hoverBorderColor: "rgba(255,99,132,1)",
                data: [],
            },
        ],
    };
    if (obj.length > 0) {
        obj.map((o) => {
            const keys = Object.keys(o);
            const monthProp = keys[0];
            const yearProp = keys[1];
            if (!o[monthProp] || !o[yearProp]) {
                return;
            }
            if (o[monthProp] && o[yearProp]) {
                model.labels.push(`${o[monthProp]}-${o[yearProp]}`);
                model.datasets[0].data.push(o.total);
            }
        });
    } else model.datasets.pop();
    return model;
}

function randomHexa() {
    return "#" + Math.random().toString(16).slice(2, 8).toUpperCase();
}

exports.getTenants = async (req, res) => {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized",
        });
    }
    const tenantResult = await utils.getTenants(
        req.user.tenants,
        /* req.user.test */ false
    );
    return res.status(200).json({
        tenants: tenantResult,
    });
};
