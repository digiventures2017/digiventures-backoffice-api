const Utils = require("../config/utils");
const TenantService = require("../services/TenantService");

class BaseController {
    constructor() {}

    async init(legajoId, isDev, includeTenant = true, includeUser = false) {
        const legajo = await Utils.getLegajo(legajoId, isDev);
        const { userId, tenantId, dev } = legajo;
        let userPromise = false;
        let tenantPromise = false;

        if (includeUser) userPromise = Utils.getUser(userId, "", dev);

        if (includeTenant) tenantPromise = TenantService.getById(tenantId, dev);

        const [user, tenant] = await Promise.all([userPromise, tenantPromise]);

        if (includeTenant && !tenant) return false;
        if (includeUser && !user) return false;

        return { tenant, legajo, user, dev };
    }
}

module.exports = BaseController;
