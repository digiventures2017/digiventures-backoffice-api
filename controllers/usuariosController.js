const config = require("../config/config"),
    Utils = require("../config/utils"),
    moment = require("moment"),
    _ = require("lodash"),
    fetch = require("node-fetch");

exports.getAll = function(req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }

    const { name, email, idNumber, datefrom, dateto, tenants } = req.query;
    const timezone = req.query.timezone
        ? req.query.timezone
        : "America/Argentina/Buenos_Aires";

    const perPage = 20;
    let page = req.query.page || 0;

    if (page >= 1) {
        page--;
    }

    let query = {};
    let stringQuery = "";

    query.dev = req.user.test;

    if (name) {
        query.name = name;
        stringQuery += "name=" + name + "&";
    }

    if (email) {
        query.email = email;
        stringQuery += "email=" + email + "&";
    }

    if (idNumber) {
        query.idNumber = idNumber.toString();
        stringQuery += "idNumber=" + query.idNumber + "&";
    }

    if (datefrom) {
        let date = moment(parseInt(datefrom))
            .tz(timezone)
            .utc()
            .toDate();

        if (!query.createdAt) {
            query.createdAt = {};
        }

        query.createdAt["$gte"] = date;

        stringQuery += "datefrom=" + datefrom + "&";
    }

    if (dateto) {
        let date = moment(parseInt(dateto))
            .tz(timezone)
            .set({ hour: 23, minute: 59, second: 59, millisecond: 59 })
            .utc()
            .toDate();

        if (!query.createdAt) {
            query.createdAt = {};
        }

        query.createdAt["$lte"] = date;

        stringQuery += "dateto=" + dateto + "&";
    }

    if (tenants) {
        let tmpSelectedTenants = tenants.split(",");

        stringQuery += "tenants=" + tmpSelectedTenants + "&";

        query.tenantId = { $in: tmpSelectedTenants };
    }

    const urlUsers = Utils.getUrlUsers(req.user.test);

    let selectFields = {
        _id: 1,
        name: 1,
        email: 1,
        idNumber: 1,
        tenantId: 1,
        updatedAt: 1,
        createdAt: 1
    };

    if (req.user.tenants.length) {
        if (!query.tenantId) {
            query.tenantId = { $in: req.user.tenants };
        }
    }

    const tenantResult = Utils.getTenants(req.user.tenants, req.user.test);

    const getUsers = Utils.getUsers(
        query,
        selectFields,
        perPage,
        page * perPage
    );

    Promise.all([getUsers, tenantResult]).then(values => {
        const users = values[0];
        const tenants = values[1];

        return res.json({
            users: users,
            tenants: tenants,
            stringQuery: stringQuery,
            query: query,
            count: users.count,
            page: page,
            perPage: perPage,
            roleUser: req.user.role
        });
    });
};

exports.deleteUser = function(req, res) {
    if (!req.user) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }

    if (req.user.role != "SuperAdministrador") {
        return;
    }

    const idUser = req.params.id;

    if (idUser) {
        return Utils.deleteUser(idUser, req.user.test)
            .then(result => {
                return res.json({
                    deleted: true
                });
            })
            .catch(err => {
                console.log("ERR CATCH: " + new Date().toString());
                console.log(err);
                console.log("-------------------------------------------");
            });
    }
};
