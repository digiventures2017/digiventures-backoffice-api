#! /bin/bash
echo "Desactivo CI"
unset CI

echo "Comprimo y subo al server"
zip -r /tmp/api-admin-dev.zip . -x node_modules\*
scp -r /tmp/api-admin-dev.zip ubuntu@$DEPLOY_HOST_API_FRONTEND:/home/ubuntu/Projects/api-admin-dev.zip

ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects; sudo rm -rf api-admin-dev; unzip -o api-admin-dev.zip -d api-admin-dev;'

echo "Stash, pull and build"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects/api-admin-dev; sudo docker-compose -f docker-compose-dev.yml up -d --build --remove-orphans;'

#echo "Remove zip and folder"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects; rm -rf api-admin-dev.zip; rm -rf api-admin-dev;'

echo "Removing orphan images"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'sudo docker rmi -f $(sudo docker images -a -q --filter "dangling=true")'
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'sudo docker rm $(sudo docker ps -a -q --filter status=exited)'

echo "Success!"

exit 0