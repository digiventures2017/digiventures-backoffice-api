let self = {};
const _ = require("lodash");
const Utils = require("./utils");

self.sendImage = async function(type, data, isDev) {
    const urlImages = Utils.getUrlImages(isDev);
    const url = urlImages + type;

    const options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    };

    const result = await Utils.fetchUrl(url, options);

    return result;
};

module.exports = self;
