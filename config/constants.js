const branchEnv = process.env.BRANCH_ENV;
const urlMongo =
    branchEnv === "local"
        ? "mongodb://localhost:27017/digiventures"
        : "mongodb://mongo-admin:27017/digiventures";
const urlRedis = branchEnv === "local" ? "localhost" : "redis";

module.exports = {
    scoringReferences: {},
    branchEnv,
    urlMongo,
    urlRedis,
};
