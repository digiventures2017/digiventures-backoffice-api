const AWS = require("aws-sdk");
const branchEnv = process.env.BRANCH_ENV;
const branchDev = process.env.BRANCH === "development";
const dev = process.env.NODE_ENV !== "production";
const config = require("./config");
const fetch = require("node-fetch");
const _ = require("lodash");
const mimetype = require("mimetype");

const self = {};

self.fetchUrl = async function (url, options = {}, full = false) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
        let response = {};
        let result = {};
        try {
            response = await fetch(url, options);
            // eslint-disable-next-line no-empty
        } catch (e) {
            return reject(e);
        }

        try {
            result = await response.json();
            // eslint-disable-next-line no-empty
        } catch (e) {}

        if (full) {
            response.body = result;
            return resolve(response);
        }
        return resolve(result);
    });
};

self.getUrlMultiTenant = function (dev) {
    if (dev) {
        if (!branchDev) {
            return config.configDev.urlMultiTenant;
        } else {
            return config.configLocal.urlMultiTenant;
        }
    } else {
        return config.configProd.urlMultiTenant;
    }
};

self.getUrlTenants = function (isDev) {
    if (branchEnv == "local") {
        return config.configLocal.urlTenants;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlTenants;
        } else {
            return config.configDev.urlTenants;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlTenants;
        } else {
            return config.configDev.urlTenants;
        }
    }
};

self.getUrlMiddle = function (isDev) {
    if (branchEnv == "local") {
        return config.configLocal.urlPublicMiddle;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlPublicMiddle;
        } else {
            return config.configDev.urlPublicMiddle;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlPublicMiddle;
        } else {
            return config.configDev.urlPublicMiddle;
        }
    }
};

self.getUrlApplications = (isDev) => {
    if (branchEnv == "local") {
        return config.configLocal.urlApplications;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlApplications;
        } else {
            return config.configDev.urlApplications;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlApplications;
        } else {
            return config.configDev.urlApplications;
        }
    }
};

self.getApplicationsJWTConfig = () => {
    return config.jwt;
};

self.getUrlUsers = function (isDev) {
    if (branchEnv == "local") {
        return config.configLocal.urlPublicUsers;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlPublicUsers;
        } else {
            return config.configDev.urlPublicUsers;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlInternalUsers;
        } else {
            return config.configDev.urlInternalUsers;
        }
    }
};

self.getUrlLegajos = function (isDev) {
    if (branchEnv == "local") {
        return config.configLocal.urlPublicLegajos;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlPublicLegajos;
        } else {
            return config.configDev.urlPublicLegajos;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlInternalLegajos;
        } else {
            return config.configDev.urlInternalLegajos;
        }
    }
};

self.getUrlImages = function (isDev) {
    if (branchEnv == "local") {
        return config.configLocal.urlPublicImages;
    }

    if (dev) {
        if (!branchDev) {
            return config.configProd.urlInternalImages;
        } else {
            return config.configDev.urlPublicImages;
        }
    } else {
        if (!isDev) {
            return config.configProd.urlInternalImages;
        } else {
            return config.configDev.urlInternalImages;
        }
    }
};

self.validateEmail = function (email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

self.getBase64ImageS3 = function (url, path) {
    return new Promise(function (resolve, reject) {
        const keyUrls = url.split("/");
        const bucket = keyUrls[3];
        const keyUrl = keyUrls.slice(4, keyUrls.length).join("/");
        const credentials = {
            accessKeyId: "AKIAJ5D7O3QEEWSAULPA",
            secretAccessKey: "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",
        };

        const s3 = new AWS.S3(credentials);

        const params = {
            Bucket: bucket,
            Key: keyUrl,
        };

        s3.getObject(params, function (err, file) {
            if (err || file === null) {
                console.error({ file, url, err });
                return resolve("");
            }
            let base64 = Buffer.from(file.Body).toString("base64");
            let _mimetype = "image/png";
            const loopkUpMimetype = mimetype.lookup(url);
            if (base64.indexOf("dataapplicationpdfbase64") != -1) {
                base64 = base64.replace(
                    "dataapplicationpdfbase64",
                    "data:application/pdf;base64,"
                );

                return resolve(base64);
            } else if (keyUrl.endsWith(".pdf")) {
                _mimetype = "application/pdf";
            } else if (loopkUpMimetype) {
                _mimetype = loopkUpMimetype;
            }

            base64 = `data:${_mimetype};base64,${base64}`;

            resolve(path ? { base64: base64, path: path } : base64);
        });
    });
};

self.deleteTenant = async function (idTenant, isDev) {
    const dev = isDev ? "dev" : "";
    const redisKey = `tenant-${idTenant}-${dev}`;
    const urlConfigTenant = self.getUrlMultiTenant(isDev);
    const url = urlConfigTenant + "multitenant_configuration?id=" + idTenant;

    const options = {
        method: "DELETE",
    };

    const tenant = await self.fetchUrl(url, options);
    try {
        await config.redis.del(redisKey);
    } catch (e) {
        console.log("ERROR DELETING TENANT ID", idTenant, e);
    }

    return tenant;
};

self.getTenant = async function (idTenant, isDev) {
    const dev = isDev ? "dev" : "";
    const redisKey = `tenant-${idTenant}-${dev}`;
    const getCache = await config.redis.get(redisKey);
    if (getCache) {
        return JSON.parse(getCache);
    }

    const urlConfigTenant = self.getUrlMultiTenant(isDev);
    const url = urlConfigTenant + "multitenant_configurationget?id=" + idTenant;

    let tenant = await self.fetchUrl(url);

    const configTenant = tenant.config;

    const arConfig = [];

    if (configTenant) {
        configTenant.map(function (config) {
            const jsonConfig = JSON.parse(config);
            arConfig.push(jsonConfig);
        });
    } else {
        tenant = {};
    }

    const objConfig = {};

    arConfig.map((config) => {
        objConfig[config.step_id] = config;
    });

    tenant.objConfig = objConfig;

    tenant.config = arConfig;

    try {
        await config.redis.set(redisKey, JSON.stringify(tenant));
    } catch (e) {
        console.log("Error setting cache");
    }

    return tenant;
};

self.getTenants = async function (tenantsUser, isDev) {
    const urlConfigTenant = self.getUrlMultiTenant(isDev);
    const url = urlConfigTenant + "multitenant_configurationgetall";

    let tenant = [];
    const dev = isDev ? "dev" : "";
    const redisKey = `tenants-${dev}`;
    const getCache = await config.redis.get(redisKey);
    if (getCache) {
        tenant = JSON.parse(getCache);
    } else {
        tenant = await self.fetchUrl(url);
        if (tenant.tenantLst)
            await config.redis.set(redisKey, JSON.stringify(tenant));
    }

    let tenantList = tenant.tenantLst || [];

    if (tenantsUser.length) {
        const tmpTenants = [];

        tenantList.forEach(function (t) {
            if (tenantsUser.indexOf(t._id) > -1) {
                tmpTenants.push(t);
            }
        });

        tenantList = tmpTenants;
    }

    const tmpTenants = {};

    tenantList.forEach(function (t) {
        tmpTenants[t._id] = t;
    });
    return tmpTenants;
};

self.getTenantsList = async function (tenantsUser, isDev) {
    const urlConfigTenant = self.getUrlMultiTenant(isDev);
    const url = urlConfigTenant + "multitenant_configurationgetall";

    let tenant = {};
    const dev = isDev ? "dev" : "";
    const redisKey = `tenants-list-${dev}`;
    const getCache = await config.redis.get(redisKey);
    if (getCache) {
        tenant = JSON.parse(getCache);
    } else {
        tenant = await self.fetchUrl(url);
        await config.redis.set(redisKey, JSON.stringify(tenant));
    }

    const tenantList = tenant.tenantLst;

    if (tenantsUser && tenantsUser.length) {
        const tmpTenants = [];

        tenantList.forEach(function (t) {
            if (tenantsUser.indexOf(t._id) > -1) {
                tmpTenants.push({ value: t._id, text: t.name_tenant });
            }
        });

        return tmpTenants;
    } else if (tenantsUser) {
        return tenantList.map((t) => {
            return { value: t._id, text: t.name_tenant };
        });
    }

    return [];
};

self.getUsers = async function (
    query,
    selectFields,
    perPage,
    offset,
    isDev,
    sort = { createdAt: -1 }
) {
    const urlLegajos = self.getUrlUsers(isDev);
    const url = `${urlLegajos}/users_query?query=${JSON.stringify(
        query
    )}&selectFields=${JSON.stringify(
        selectFields
    )}&limit=${perPage}&offset=${offset}&sort=${JSON.stringify(sort)}`;

    const result = await self.fetchUrl(url);

    return result;
};

self.getLegajos = async function (
    query,
    selectFields,
    perPage,
    offset,
    isExport = false,
    isDev = false,
    sorting = {}
) {
    const tmpQuery = _.cloneDeep(query);
    delete tmpQuery.legajoComplete;

    const urlLegajos = self.getUrlLegajos(isDev);
    const url = `${urlLegajos}${
        isExport ? "/legajos_query" : "/es/legajos"
    }?query=${JSON.stringify(tmpQuery)}&selectFields=${JSON.stringify(
        selectFields
    )}&limit=${perPage}&offset=${offset}&sorting=${JSON.stringify(sorting)}`;
    const result = await self.fetchUrl(url);
    return result;
};

self.getLegajo = async function (idLegajo, isDev) {
    const urlLegajos = self.getUrlLegajos(isDev);
    const url = urlLegajos + "/legajos?id=" + idLegajo;

    const result = await self.fetchUrl(url);

    return result;
};

self.getAllLegajos = async function (hash, isDev = false) {
    const urlLegajos = self.getUrlLegajos(isDev);
    const url = urlLegajos + "/fetch_all/" + hash;

    const result = await self.fetchUrl(url);

    return result;
};

self.getNavigation = async function (idLegajo, isDev) {
    const urlLegajos = self.getUrlLegajos(isDev);
    const url = urlLegajos + "/navigation/" + idLegajo;

    const result = await self.fetchUrl(url);

    return result;
};

self.getUser = async function (idUser, data, isDev) {
    const urlUsers = self.getUrlUsers(isDev);
    const url = urlUsers + "/users?idUser=" + idUser;

    const result = await self.fetchUrl(url);

    return result;
};

self.updateUser = async function (idUser, data, isDev) {
    const urlUsers = self.getUrlUsers(isDev);
    const url = urlUsers + "/users?idUser=" + idUser;

    const options = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    };

    const result = await self.fetchUrl(url, options);

    return result;
};

self.updateLegajo = async function (
    idLegajo,
    data,
    isDev,
    customProp = "id",
    full = false,
    customHeaders = {}
) {
    const urlLegajo = self.getUrlLegajos(isDev);
    const url = urlLegajo + `/legajos?${customProp}=${idLegajo}`;

    const options = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            from: "onboarding-backoffice",
            ...customHeaders,
        },
        body: JSON.stringify(data),
    };

    const result = await self.fetchUrl(url, options, full);

    return result;
};

self.updateLegajosByUserId = async function (userId, data, isDev, full) {
    const urlLegajo = self.getUrlLegajos(isDev);
    const url = urlLegajo + `/legajos_by_user/${userId}`;

    const options = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    };

    const result = await self.fetchUrl(url, options, full);
    result.userId = userId;
    return result;
};

self.deleteLegajo = async function (idLegajo, isDev) {
    const urlLegajo = self.getUrlLegajos(isDev);
    const url = urlLegajo + "/legajo/" + idLegajo;

    const options = {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            from: "onboarding-backoffice",
        },
    };

    const result = await self.fetchUrl(url, options);

    return result;
};

self.deleteUser = async function (idUser, isDev) {
    const urlUsers = self.getUrlUsers(isDev);
    const url = urlUsers + "/user/" + idUser;

    const options = {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
    };

    const result = await self.fetchUrl(url, options);

    return result;
};

self.putBase64ImageS3 = function (
    key,
    base64,
    path = "landing",
    read = "public-read"
) {
    return new Promise(function (resolve, reject) {
        let buf = "";
        let extension = ".png";
        let type = "image/png";
        if (base64.includes(",")) {
            const splitted = base64.split(",");
            buf = splitted[1];
            const groups = splitted[0].match(/(video\/)(.*.)(;)/);
            if (splitted[0].includes("application")) {
                extension = ".pdf";
                type = "application/pdf";
            } else if (groups && groups[2]) {
                extension = `.${groups[2]}`;
                type = `${groups[1]}${groups[2]}`;
            }
        }
        buf = Buffer.from(buf, "base64");
        const uuid = self.generateUUID();

        const credentials = {
            accessKeyId: "AKIAJ5D7O3QEEWSAULPA",
            secretAccessKey: "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",
        };

        const s3 = new AWS.S3(credentials);
        const filename = path + "/" + uuid + extension;
        const params = {
            Bucket: "test.digiventures",
            Body: buf,
            Key: filename,
            ContentEncoding: "base64",
            ContentType: type,
        };

        if (read === "public-read") {
            params.ACL = read;
        }

        s3.putObject(params, function (err) {
            if (err) {
                reject(err);
            }

            resolve({
                key: key,
                url: "https://s3.amazonaws.com/test.digiventures/" + filename,
            });
        });
    });
};

self.generateUUID = function () {
    let d = new Date().getTime();
    const uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
        /[xy]/g,
        function (c) {
            const r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
        }
    );
    return uuid;
};

self.getIndicesOf = function (searchStr, str, caseSensitive) {
    const searchStrLen = searchStr.length;
    if (searchStrLen == 0) {
        return [];
    }
    let startIndex = 0,
        index;
    const indices = [];
    if (!caseSensitive) {
        str = str.toLowerCase();
        searchStr = searchStr.toLowerCase();
    }
    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    return indices;
};

self.getScoringManagement = (scoringManagement, idScoringSearch) => {
    for (const index in scoringManagement) {
        const scoring = scoringManagement[index];

        if (scoring.idScoring == idScoringSearch) {
            return scoring;
        }
    }
};

self.buildLegajoStatusStructure = (tenant) => {
    let scoringStatusManagement = _.get(
        tenant,
        "constants.scoringStatusManagement.arr"
    );
    let scoringManagement = _.get(tenant, "constants.scoringManagement.arr");

    const structure = {};

    scoringStatusManagement.forEach((scoringStatus) => {
        if (!structure[scoringStatus.idScoring]) {
            const tmpScoringManagement = self.getScoringManagement(
                scoringManagement,
                scoringStatus.idScoring
            );

            structure[scoringStatus.idScoring] = {
                name: tmpScoringManagement.name,
                type: tmpScoringManagement.type,
                statusScoring: [],
                subStatusScoring: [],
                completenessLegajo: [],
            };
        }

        if (
            scoringStatus.statusScoring &&
            structure[scoringStatus.idScoring].statusScoring.indexOf(
                scoringStatus.statusScoring
            ) === -1
        ) {
            structure[scoringStatus.idScoring].statusScoring.push(
                scoringStatus.statusScoring
            );
        }

        if (
            scoringStatus.subStatusScoring &&
            structure[scoringStatus.idScoring].subStatusScoring.indexOf(
                scoringStatus.subStatusScoring
            ) === -1
        ) {
            structure[scoringStatus.idScoring].subStatusScoring.push(
                scoringStatus.subStatusScoring
            );
        }

        if (
            scoringStatus.completenessLegajo &&
            structure[scoringStatus.idScoring].completenessLegajo.indexOf(
                scoringStatus.completenessLegajo
            ) === -1
        ) {
            structure[scoringStatus.idScoring].completenessLegajo.push(
                scoringStatus.completenessLegajo
            );
        }
    });

    const finalStructure = [];

    for (const index in structure) {
        finalStructure.push(structure[index]);
    }

    return finalStructure;
};

self.getStatusLegajo = (legajo, statusStructure) => {
    let legajoStatusText = config.legajoStatusText;

    if (statusStructure) {
        legajoStatusText = statusStructure;
    }

    for (const index in legajoStatusText) {
        const status = legajoStatusText[index];

        let isStatus = true;

        if (status.statusScoring != legajo.statusScoring) {
            isStatus = false;
        }

        if (
            legajo.subStatusScoring &&
            Array.isArray(status.subStatusScoring) &&
            status.subStatusScoring.length &&
            status.subStatusScoring.indexOf(legajo.subStatusScoring) == -1
        ) {
            isStatus = false;
        }

        if (
            legajo.completenessLegajo &&
            Array.isArray(status.completenessLegajo) &&
            status.completenessLegajo.length &&
            status.completenessLegajo.indexOf(legajo.completenessLegajo) == -1
        ) {
            isStatus = false;
        }

        if (isStatus) {
            return status;
        }
    }
};

self.addProps = (obj, arr, val) => {
    if (typeof arr == "string") arr = arr.split(".");

    obj[arr[0]] = obj[arr[0]] || {};

    const tmpObj = obj[arr[0]];

    if (arr.length > 1) {
        arr.shift();
        self.addProps(tmpObj, arr, val);
    } else obj[arr[0]] = val;

    return obj;
};

self.getConfigStep = (tenant, stepId, typeScoring) => {
    let configStep = {};

    if (
        typeScoring &&
        stepId !== "landing" &&
        stepId !== "data" &&
        stepId !== "Rechazado"
    ) {
        configStep = tenant.steps[stepId][typeScoring];
    } else {
        configStep = tenant.steps[stepId];
    }

    return configStep;
};

self.getBigQueryTable = function () {
    if (branchDev) {
        return "onboarding_legajos"; /* "test"; */
    } else {
        return "onboarding_legajos";
    }
};

self.getCompletenessLegajoKeyString = (value) => {
    let key = "Undefined";
    Object.keys(config.completenessLegajo).map((k) => {
        if (config.completenessLegajo[k] == value) {
            key = k;
        }
    });
    return key;
};

self.checkExistingErrorCode = (errors, arrayErrorsCode) => {
    if (errors && typeof errors == "object" && arrayErrorsCode) {
        for (let index = 0; index < errors.length; index++) {
            const element = errors[index];
            if (arrayErrorsCode.indexOf(element["code"]) > -1) {
                return false;
            }
        }
    } else if (typeof errors == "number") {
        return false;
    }
    return true;
};

self.query = (args) => {
    return (
        "?" +
        Object.entries(args)
            .map(([key, value]) => {
                return key + "=" + value;
            })
            .join("&")
    );
};

self.getCompletenessLegajoKeyString = (value) => {
    let key = "Undefined";
    Object.keys(config.completenessLegajo).map((k) => {
        if (config.completenessLegajo[k] == value) {
            key = k;
        }
    });
    return key;
};

self.replaceWithData = (data, text) => {
    if (!text) return "";
    if (typeof text != "string") return text;
    const matches = text.match(new RegExp("{(.*?)}+", "g"));
    if (matches) {
        matches.map((m) => {
            let value = _.get(data, m.replace(/{|}/g, ""));
            if (typeof value == "undefined") value = "";
            text = text.replace(new RegExp(m, "g"), value);
        });
    }
    return text;
};

self.getKeys = (text, double = false) => {
    const regex = double ? "{{(.*?)}}+" : "{(.*?)}+";
    if (!text) return "";
    if (typeof text != "string") return text;
    const matches = text.match(new RegExp(regex, "g"));
    return matches;
};

module.exports = self;
