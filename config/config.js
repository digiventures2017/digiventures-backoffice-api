const self = {};
const getConfig = require("./getConfig");
const { urlMongo } = require("./constants");

self.clientRedis;
self.arTenants = {};

const IP_API_MONGO = "172.31.88.43";

self.configProd = {
    urlMultiTenant: "http://172.31.84.77:9090/apiMultitenant/",
    //urlMultiTenant: "http://35.153.108.44:9090/apiMultitenant/",
    urlInternalUsers: "http://" + IP_API_MONGO + ":3535",
    urlPublicUsers: "http://" + IP_API_MONGO + ":3535",
    urlInternalLegajos: "http://" + IP_API_MONGO + ":3636",
    urlPublicLegajos: "http://" + IP_API_MONGO + ":3636",
    urlPublicMiddle: "https://api.digiventures.com.ar",
    urlInternalImages: "http://172.31.84.77:7070/photoservice",
    urlPublicImages: "http://35.153.108.44:7070/photoservice",
    urlApplications: "https://webhook.digiventures.com.ar",
    urlTenants: "https://api-tenants.digiventures.la",
    urlInternalScoring: "http://172.31.84.77:6060/scoring/v2/score",
    urlPublicScoring: "http://35.153.108.44:6060/scoring/v2/score",
    urlScore: "https://api-score.digiventures.la/trigger",
};

self.configDev = {
    urlMultiTenant: "http://172.31.43.144:9090/apiMultitenant/",
    //urlMultiTenant: "http://54.83.138.6:9090/apiMultitenant/",
    urlInternalUsers: "http://" + IP_API_MONGO + ":3535",
    urlPublicUsers: "http://" + IP_API_MONGO + ":3535",
    urlInternalLegajos: "http://" + IP_API_MONGO + ":3636",
    urlPublicLegajos: "http://" + IP_API_MONGO + ":3636",
    urlPublicMiddle: "https://api-dev.digiventures.com.ar",
    urlInternalImages: "http://172.31.43.144:7070/photoservice",
    urlPublicImages: "http://54.83.138.6:7070/photoservice",
    urlApplications: "https://webhook.digiventures.com.ar",
    urlTenants: "https://api-tenants-ci.digiventures.la",
    urlInternalScoring: "http://172.31.43.144:6060/scoring/v2/score",
    urlPublicScoring: "http://54.83.138.6:6060/scoring/v2/score",
    urlScore: "https://api-score.digiventures.la/trigger",
};

self.configLocal = {
    //urlMultiTenant: "http://54.83.138.6:9090/apiMultitenant/",
    urlMultiTenant: "http://54.83.138.6:9090/apiMultitenant/",
    urlInternalUsers: "http://localhost:3535",
    urlPublicUsers: "http://localhost:3535",
    urlInternalLegajos: "http://localhost:3636",
    urlPublicLegajos: "http://localhost:3636",
    urlPublicMiddle: "http://localhost:7979",
    urlInternalImages: "http://172.31.43.144:7070/photoservice",
    urlPublicImages: "http://54.83.138.6:7070/photoservice",
    urlApplications: "http://localhost:5100",
    //urlTenants: "https://api-tenants-ci.digiventures.la",
    urlTenants: "http://localhost:2700",
    urlInternalScoring: "http://172.31.43.144:6060/scoring/v2/score",
    urlPublicScoring: "http://54.83.138.6:6060/scoring/v2/score",
    urlScore: "https://api-score.digiventures.la/trigger",
    // urlScore: "http://localhost:7777/trigger",
};

self.jwt = {
    secret: "ecpbXt0jJWnYlUGattHIph0IMxnjJ90jXx5lUeWU6zJGtPTqI8f860G7LBJH",
    hash: "LTuTHfTR9yxeIT6dSp5s1MhsNw7038RRNJLLjwFsXEyJZiwh9rXid9jXDDCj",
};

self.typeScoring = [
    "Directo",
    "RevisionManual",
    "Recurrente",
    "Rechazado",
    "Contactar",
    "SinScore",
];

self.statusLegajo = {
    APROBADO: 1,
    RECHAZADO: 2,
    PENDING: 3,
    1: "APROBADO",
    2: "RECHAZADO",
    3: "PENDING",
};

self.subStatusLegajo = {
    DIRECTO: 1,
    REVISION_MANUAL: 2,
    RECURRENTE: 3,
    RECHAZADO: 4,
    CONTACTAR: 5,
    SIN_SCORE: 6,
    1: "DIRECTO",
    2: "REVISION_MANUAL",
    3: "RECURRENTE",
    4: "RECHAZADO",
    5: "CONTACTAR",
    6: "SIN_SCORE",
};

self.completenessLegajo = {
    COMPLETO: 1,
    INCOMPLETO: 2,
    CON_ERRORES: 3,
    1: "COMPLETO",
    2: "INCOMPLETO",
    3: "CON_ERRORES",
};

self.nameStatusLegajo = {
    CALIFICA: "CALIFICA",
    CONTACTO: "CONTACTO",
    APROBADO: "APROBADO",
    RECHAZADO: "RECHAZADO",
    PENDING: "PENDING",
    DIRECTO: "DIRECTO",
    REVISION_MANUAL: "REVISION MANUAL",
    RECURRENTE: "RECURRENTE",
    CONTACTAR: "CONTACTAR",
    SIN_SCORE: "SIN SCORE",
    COMPLETO: "COMPLETO",
    INCOMPLETO: "INCOMPLETO",
    CON_ERRORES: "CON ERRORES",
};

self.typeScoringStatus = {
    Directo: {
        statusLegajo: self.statusLegajo.APROBADO,
        subStatusLegajo: self.subStatusLegajo.DIRECTO,
    },
    RevisionManual: {
        statusLegajo: self.statusLegajo.APROBADO,
        subStatusLegajo: self.subStatusLegajo.REVISION_MANUAL,
    },
    Recurrente: {
        statusLegajo: self.statusLegajo.APROBADO,
        subStatusLegajo: self.subStatusLegajo.RECURRENTE,
    },
    Rechazado: {
        statusLegajo: self.statusLegajo.RECHAZADO,
        subStatusLegajo: self.subStatusLegajo.RECHAZADO,
    },
    Contactar: {
        statusLegajo: self.statusLegajo.PENDING,
        subStatusLegajo: self.subStatusLegajo.CONTACTAR,
    },
    SinScore: {
        statusLegajo: self.statusLegajo.PENDING,
        subStatusLegajo: self.subStatusLegajo.SIN_SCORE,
    },
};

self.legajoStatusText = [
    {
        name: "Completo",
        statusScoring: [self.statusLegajo.APROBADO],
        subStatusScoring: [
            self.subStatusLegajo.DIRECTO,
            self.subStatusLegajo.REVISION_MANUAL,
            self.subStatusLegajo.RECURRENTE,
        ],
        completenessLegajo: [self.completenessLegajo.COMPLETO],
    },
    {
        name: "Aprobado",
        statusScoring: [self.statusLegajo.APROBADO],
        subStatusScoring: [
            self.subStatusLegajo.DIRECTO,
            self.subStatusLegajo.REVISION_MANUAL,
            self.subStatusLegajo.RECURRENTE,
        ],
        completenessLegajo: [
            self.completenessLegajo.INCOMPLETO,
            self.completenessLegajo.CON_ERRORES,
        ],
    },
    {
        name: "Rechazado",
        statusScoring: [self.statusLegajo.RECHAZADO],
        subStatusScoring: [self.subStatusLegajo.RECHAZADO],
        completenessLegajo: [
            self.completenessLegajo.COMPLETO,
            self.completenessLegajo.INCOMPLETO,
            self.completenessLegajo.CON_ERRORES,
        ],
    },
    {
        name: "A contactar",
        statusScoring: [self.statusLegajo.PENDING],
        subStatusScoring: [self.subStatusLegajo.CONTACTAR],
        completenessLegajo: [
            self.completenessLegajo.COMPLETO,
            self.completenessLegajo.INCOMPLETO,
            self.completenessLegajo.CON_ERRORES,
        ],
    },
    {
        name: "Sin score",
        statusScoring: [self.statusLegajo.PENDING],
        subStatusScoring: [self.subStatusLegajo.SIN_SCORE],
        completenessLegajo: [
            self.completenessLegajo.COMPLETO,
            self.completenessLegajo.INCOMPLETO,
            self.completenessLegajo.CON_ERRORES,
        ],
    },
    {
        name: "Pendiente",
        statusScoring: "null",
        subStatusScoring: "null",
        completenessLegajo: "null",
    },
];

self.imagesEndpoints = {
    idCardFrontUrlBase: "/id_card_front",
    idCardBackUrlBase: "/id_card_back",
    photoUrlBase: "/photo",
    photoValidationUrlBase: "/photo_validation",
    payslipUrl: "/payslip",
    utilityUrl: "/utility",
    cbuUrl: "/cbu",
};

self.codesToCheck = {
    front: {
        quality: [2001, 2002, 3001, 3002, 3010],
        isLicense: [2003, 2006, 2012, 3001, 3002, 3010],
        sameDni: [2004, 2005, 3001, 3002, 3010],
    },
    back: {
        quality: [2005, 2006, 3001, 3002, 3010],
        ine: {
            data: [2013],
            elector_code: [2015],
            vote: [2014],
        },
    },
    photo: {
        quality: [2001, 2002, 3001, 3002, 3010],
        confidence: [2007, 3001, 3002, 3010],
        renaper: [2009, 3012],
    },
    photo_validation: {
        qualityValidation: [2001, 2002, 2010, 3001, 3002, 3010, 3011],
        grinValidation: [2001, 2011, 3001, 3002, 3010, 3011],
    },
};

self.scoringConfiguration = {
    references: {
        Directo: { type: "primary", label: "Directo" },
        RevisionManual: {
            type: "primary",
            label: "Revisión manual",
        },
        Recurrente: {
            type: "primary",
            label: "Recurrente",
        },
        Rechazado: { type: "danger", label: "Rechazado" },
        Contactar: { type: "warning", label: "Contactar" },
        SinScore: {
            type: "secondary",
            label: "Sin score",
        },
    },
};

self.apiScoringVersion = {
    radar: "v2",
    infocore: "v2",
    default: "v1",
};

self.mongoConfig = {
    mongodb: urlMongo, // MongoDB Connection URI
    s3: {
        accessKey: "AKIAJ5D7O3QEEWSAULPA", //AccessKey
        secretKey: "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", //SecretKey
        region: "us-east-1", //S3 Bucket Region
        accessPerm: "private", //S3 Bucket Privacy, Since, You'll be storing Database, Private is HIGHLY Recommended
        bucketName: "test.digiventures/backup-mongodb/admin", //Bucket Name
    },
    keepLocalBackups: false, //If true, It'll create a folder in project root with database's name and store backups in it and if it's false, It'll use temporary directory of OS
    noOfLocalBackups: 5, //This will only keep the most recent 5 backups and delete all older backups from local backup directory
    timezoneOffset: 300, //Timezone, It is assumed to be in hours if less than 16 and in minutes otherwise
};

self.sendgrid = {
    from: "contacto@digiventures.com.ar",
    apiKey: "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",
    to: [
        "ignacio.salle@digiventures.com.ar",
        "yamila.klausen@digiventures.com.ar",
        "franco.romeo@digiventures.com.ar",
        "agustina.gianera@digiventures.com.ar",
    ],
};

self.back_url = getConfig(
    "",
    `https://admin.digiventures.la`,
    `https://admin-test.digiventures.la`,
    `http://localhost:3001`
);

self.auth = {
    google: {
        clientID: getConfig(
            "",
            "1068804891673-uqu431eqjnpkhb5nakkprvbmbmia3ie7.apps.googleusercontent.com"
        ),
        clientSecret: getConfig("", "6EBYaPLIWqGB9hohCZpGVyCg"),
        callbackURL: getConfig(
            "",
            `https://api-backoffice.digiventures.la/auth/google/callback`,
            `https://api-backoffice-dev.digiventures.la/auth/google/callback`,
            `http://localhost:5000/auth/google/callback`
        ),
        passReqToCallback: true,
    },
    facebook: {
        clientID: getConfig("", "613680555825501"),
        clientSecret: getConfig("", "6f62369eb581dabc9711716c9729bbe2"),
        callbackURL: getConfig(
            "",
            `https://api-backoffice.digiventures.la/auth/facebook/callback`,
            `https://api-backoffice-dev.digiventures.la/auth/facebook/callback`,
            `http://localhost:5000/auth/facebook/callback`
        ),
        passReqToCallback: true,
        profileFields: ["id", "emails", "name"],
    },
};

self.html = {
    allowedTags: [
        "html",
        "head",
        "style",
        "body",
        "address",
        "article",
        "aside",
        "footer",
        "header",
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6",
        "hgroup",
        "main",
        "nav",
        "section",
        "blockquote",
        "dd",
        "div",
        "dl",
        "dt",
        "figcaption",
        "figure",
        "hr",
        "li",
        "main",
        "ol",
        "p",
        "pre",
        "ul",
        "a",
        "abbr",
        "b",
        "bdi",
        "bdo",
        "br",
        "cite",
        "code",
        "data",
        "dfn",
        "em",
        "i",
        "kbd",
        "mark",
        "q",
        "rb",
        "rp",
        "rt",
        "rtc",
        "ruby",
        "s",
        "samp",
        "small",
        "span",
        "strong",
        "sub",
        "sup",
        "time",
        "u",
        "var",
        "wbr",
        "caption",
        "table",
        "tbody",
        "td",
        "tfoot",
        "th",
        "thead",
        "tr",
        "img",
    ],
    allowedAttributes: {
        "*": ["style", "class", "colspan", "rowspan", "src", "width", "height"],
    },
};

module.exports = self;
