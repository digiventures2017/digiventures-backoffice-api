const schedule = require("node-schedule");
const _ = require("lodash");
// const stringify = require("stringify-keys");
const generateBigQueryModelSchema = require("./generateBigQueryModelSchema");
const moment = require("moment");
const projectId = "digiventures";

const bqObj = {
    projectId: projectId,
    keyFilename: "./bigquery/bigquery-key.json",
};
const { BigQuery } = require("@google-cloud/bigquery");
const bigquery = new BigQuery(bqObj);

//slack
const SLACK_WEBHOOK_URL =
    "https://hooks.slack.com/services/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";
const slack = require("slack-notify")(SLACK_WEBHOOK_URL);

function sincronize(
    model,
    dataset,
    params = {
        dev: "none",
        time: "0 3 * * *",
        predefined: [],
        excluded: [],
        limit: 100,
        searchQuery: {},
        fieldSearchTime: "updatedAt",
    }
) {
    console.log(params);
    if (!process.env.SYNC_BQ) return;
    params = {
        dev: "none",
        time: "0 3 * * *",
        predefined: [],
        excluded: [],
        searchQuery: {},
        fieldSearchTime: "updatedAt",
        ...params,
    };
    const dev = _.get(params, "dev") || "none";
    const time = _.get(params, "time") || "0 3 * * *";
    const predefined = _.get(params, "predefined") || [];
    const excluded = _.get(params, "excluded") || [];
    const limit = _.get(params, "limit") || 100;
    const sort = _.get(params, "sort") || {};
    const searchQueryObj = _.get(params, "searchQuery", {});
    console.log(searchQueryObj);
    schedule.scheduleJob(time, () => {
        const fieldSearchTime = _.get(params, "fieldSearchTime");

        const searchQuery = {
            ...searchQueryObj,
            [fieldSearchTime]: {
                $gte: moment().startOf("hour").subtract(2, "months"),
                $lte: moment().startOf("hour"),
            },
        };

        run(
            model,
            dataset,
            searchQuery,
            dev,
            predefined,
            excluded,
            limit,
            sort
        );
    });
}

async function run(
    model,
    dataset,
    searchQuery,
    dev = "none",
    predefined = [],
    excluded = [],
    limit = 500,
    sort = {}
) {
    excluded = excluded.join("|");
    let total = 0;
    const table = generateBigQueryModelSchema(model, predefined, excluded);
    const schema = Object.keys(table.schema).map((key) => {
        return { name: key, ...table.schema[key] };
    });
    const datasetId = `${dataset || "onboarding"}_${
        model.modelName || "undefined"
    }`;
    const tableId = `${moment().startOf("hour").format("YYYY_MM_DDTHH_mm")}`;

    let createdDataset = false;
    let createdTable = false;

    slackSend(
        `🧙‍♂️ STARTED: BigQuery synchronization on ${datasetId}.${tableId}.`
    );

    try {
        createdDataset = await createDataset(datasetId);
    } catch (e) {
        createdDataset = await getDataset(datasetId);
    } finally {
        if (!createdDataset) return;
    }

    try {
        slackSend(`❕ INFO: creating table ${datasetId}, ${tableId}`);
        createdTable = await createTable(datasetId, tableId, schema, bigquery);
        slackSend(`✅ SUCCESS: table  ${datasetId}, ${tableId} created.`);
    } catch (e) {
        slackSend(
            `❗ ERROR: creating table ${datasetId}, ${tableId} | Reason: ${e.toString()}`
        );
    } finally {
        if (!createdTable) {
            slackSend(`❕ INFO: getting table ${datasetId}, ${tableId}`);
            try {
                createdTable = await getTable(datasetId, tableId);
                slackSend(`✅ SUCCESS: table ${datasetId}, ${tableId} getted.`);
            } catch (e) {
                slackSend(
                    `🔥 SYNC CANCELLED: error getting table ${datasetId}, ${tableId} | Reason: ${e.toString()}`
                );
                return;
            }
        }
    }

    try {
        slackSend(
            `❕ INFO: starting table ${datasetId}, ${tableId} syncronization.`
        );
        await syncAll();
        slackSend(
            `👏 FINISHED: ${datasetId}.${tableId} synchronized with ${total} results`
        );
    } catch (e) {
        slackSend(
            `🔥 SYNC ERROR: ${datasetId}.${tableId}. | Reason: ${e.toString()}`
        );
    }

    async function syncAll() {
        let skip = 0;
        let data = false;
        do {
            try {
                data = await fetchModel();
                if (!data) {
                    return;
                } else if (data.results.length > 0) {
                    try {
                        /* const insertResult = */ await createdTable.insert(
                            data.results
                        );
                    } catch (e) {
                        slackSend(
                            `🔥 ERROR: BigQuery Insert error  on ${datasetId}.${tableId}. Stopping... | Reason: ${e.toString()}`
                        );
                        return;
                    }
                    skip = data.nextSkip;
                    total = total + data.results.length;
                }
            } catch (e) {
                slackSend(
                    `🔴 ERROR: model fetch. | Reason: ${JSON.stringify(e)}`
                );
                return;
            }
        } while (data.results.length > 0);

        async function fetchModel() {
            return new Promise((resolve, reject) => {
                if (dev != "none") {
                    searchQuery.dev = dev;
                }

                model
                    .find(searchQuery)
                    .skip(skip)
                    .sort(sort)
                    .limit(limit)
                    .exec((err, results) => {
                        if (err) {
                            slackSend(
                                `🔴 ERROR: query fetch ${datasetId}.${tableId}. | Reason: ${err.toString()}`
                            );
                            return reject({
                                status: "rejected",
                                from: "query",
                                error: err,
                                results: [],
                            });
                        }

                        let _id = "";
                        try {
                            const _results = results.map((result, index) => {
                                let _result = {};
                                _id = result._id.toString();

                                table.keys.map((km) => {
                                    let val = _.get(result, km);
                                    if (
                                        typeof val !== "undefined" &&
                                        val != null
                                    ) {
                                        if (km == "_id") {
                                            val = val.toString();
                                        } else if (
                                            table.schema[km].type ===
                                                "DATETIME" &&
                                            val
                                        )
                                            val = val
                                                .toISOString()
                                                .replace(
                                                    /([^T]+)T([^\.]+).*/g,
                                                    "$1 $2"
                                                );
                                        else if (
                                            table.schema[km].type === "INTEGER"
                                        )
                                            val = parseInt(val);
                                        else if (
                                            table.schema[km].type ===
                                                "STRING" &&
                                            typeof val == "object"
                                        ) {
                                            val = null;
                                        }
                                    } else {
                                        val = null;
                                    }
                                    _result[km.replace(/\./g, "_")] = val;
                                });
                                _id = "";
                                return _result;
                            });
                            return resolve({
                                results: _results,
                                nextSkip: skip + limit,
                                limit: limit,
                            });
                        } catch (e) {
                            try {
                                slackSend(
                                    `🔴 ERROR: query fetch ${datasetId}.${tableId}. | Reason: ${JSON.stringify(
                                        e
                                    )}`
                                );
                            } catch (e) {}

                            return reject({
                                status: "rejected",
                                from: "conversion",
                                entityId: _id,
                                error: e,
                                results: [],
                            });
                        }
                    });
            });
        }
    }

    // async function query(query) {
    //     const options = {
    //         query: query,
    //         location: "US",
    //         useLegacySql: true,
    //     };

    //     const [job] = await bigquery.createQueryJob(options);
    //     const [rows] = await job.getQueryResults();

    //     return rows;
    // }

    // async function deleteTable(datasetId, tableId) {
    //     const dataset = bigquery.dataset(datasetId);
    //     const [table] = await dataset.table(tableId).delete();
    //     return table;
    // }

    async function getTable(datasetId, tableId) {
        const dataset = bigquery.dataset(datasetId);
        const [table] = await dataset.table(tableId).get();

        return table;
    }

    async function createDataset(datasetId) {
        const options = {
            location: "US",
        };

        const [dataset] = await bigquery.createDataset(datasetId, options);
        return dataset;
    }

    async function getDataset(datasetId) {
        const options = {
            location: "US",
        };

        const [dataset] = await bigquery.getDatasets(datasetId, options);
        return dataset;
    }

    // async function copyTable(
    //     srcDatasetId,
    //     srcTableId,
    //     destDatasetId,
    //     destTableId
    // ) {
    //     const metadata = {
    //         createDisposition: "CREATE_IF_NEEDED",
    //         writeDisposition: "WRITE_TRUNCATE",
    //     };

    //     const [job] = await bigquery
    //         .dataset(srcDatasetId)
    //         .table(srcTableId)
    //         .copy(bigquery.dataset(destDatasetId).table(destTableId), metadata);

    //     const errors = job.status.errors;
    //     if (errors && errors.length > 0) {
    //         throw errors;
    //     }

    //     return job;
    // }

    async function createTable(datasetId, tableId, schema, bigquery) {
        const options = {
            autoCreate: true,
            schema: schema,
            location: "US",
        };

        const [table] = await bigquery
            .dataset(datasetId)
            .createTable(tableId, options);
        return table;
    }
}

// function hasJsonStructure(str) {
//     if (typeof str !== "string") return false;
//     try {
//         const result = JSON.parse(str);
//         const type = Object.prototype.toString.call(result);
//         return type === "[object Object]" || type === "[object Array]";
//     } catch (err) {
//         return false;
//     }
// }

function slackSend(text) {
    slack.send({
        channel: "#bigquery",
        icon_url:
            "https://digiventures.la/wp-content/uploads/2018/03/Logo-v1-BBW-Square-Logo.png",
        text: text,
        username: "BigQuery",
    });
}

module.exports = sincronize;
