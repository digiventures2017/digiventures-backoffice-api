function generateBigQueryModelSchema(
    SchemaModel,
    predefined = {},
    excluded = []
) {
    const mongoose = require("mongoose");
    const Schema = mongoose.Schema;
    const stringify = require("stringify-keys");
    const _ = require("lodash");

    const predefinedKeys = Object.keys(predefined);

    const bqSchema = {
        keys: ["_id", "createdAt", "updatedAt", ...predefinedKeys],
        schema: {
            _id: { type: "STRING", required: true },
            createdAt: { type: "DATETIME", required: false },
            updatedAt: { type: "DATETIME", required: false },
            ...predefined,
        },
        mixed: [],
    };

    Object.keys(bqSchema.schema).map((s) => {
        bqSchema.schema[s].name = s.replace(/\./g, "_");
    });

    function getFinalType(type) {
        switch (type) {
            case "number":
                return "INTEGER";
            case "date":
                return "DATETIME";
            case "boolean":
                return "BOOLEAN";
            default:
                return "STRING";
        }
    }

    const model = SchemaModel.schema.obj;
    const st = stringify(model).filter(
        (fst) => excluded.match(new RegExp(fst)) === null
    );
    st.map((s) => {
        if (
            excluded.match(new RegExp("default")) !== null ||
            excluded.match(new RegExp("type")) !== null
        )
            return;
        let type = _.get(model, `${s}.type`) || _.get(model, `${s}`);
        if (!type) type = String;
        if (
            type.toString() == "true" ||
            type.toString() == "false" ||
            type == "id"
        )
            return;
        const isMixed = type.name.toLowerCase() == "mixed" ? true : false;
        type = getFinalType(type.name.toLowerCase());
        let required = _.get(model, `${s}.required`);
        if (!required) required = false;
        let defaultValue = _.get(model, `${s}.default`);
        if (!defaultValue) defaultValue = false;
        let data = {
            name: s.replace(/\./g, "_"),
            type: type,
            required: required,
        };
        if (defaultValue && type != "DATETIME") {
            data.default = defaultValue;
        }
        bqSchema.keys.push(s);
        bqSchema.schema[s] = data;
        if (isMixed) bqSchema.mixed.push(s);
    });

    return bqSchema;
}

module.exports = generateBigQueryModelSchema;
