const express = require("express"),
    app = express(),
    port = process.env.PORT || 5000,
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    cors = require("cors"),
    cookieParser = require("cookie-parser"),
    passport = require("passport"),
    flash = require("express-flash"),
    cookieSession = require("cookie-session"),
    config = require("./config/config"),
    redis = require("redis"),
    RedisService = require("./services/RedisService"),
    { scheduleBackup } = require("onboarding/services/backup"),
    { branchEnv, urlMongo, urlRedis } = require("./config/constants");

const AdminUsers = require("./models/adminUsers");

AdminUsers.on("index", function (err) {
    if (err) {
        console.error("User index error: %s", err);
    } else {
        console.info("User indexing complete");
    }
});

config.clientRedis = redis.createClient(6379, urlRedis);
config.redis = new RedisService();

mongoose.connect(urlMongo, { useNewUrlParser: true });

mongoose.Promise = global.Promise;

// Mongo Backup
if (branchEnv !== "local" && process.env.NODE_ENV == "production") {
    require("newrelic");
    scheduleBackup(config.mongoConfig);
}

// Middlewares
app.use(cors({ origin: true, credentials: true }));
app.use(
    bodyParser.urlencoded({
        extended: true,
        limit: "50mb",
    })
);
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cookieParser());
app.use(
    cookieSession({
        name: "session",
        keys: ["keykeykey"],
        // Cookie Options
        maxAge: 24 * 60 * 60 * 1000, // 24 hours
    })
);

app.use((req, res, next) => {
    req.start = Date.now();
    next();
});

app.use((req, res, next) => {
    if (req.query.back_url) req.session.back_url = req.query.back_url;
    next();
});

// Route
app.get("/delete/cache", async (req, res) => {
    try {
        const keys = await config.redis.keys();
        keys.map(async (k) => {
            await config.redis.del(k);
        });
        return res.json({ keys });
    } catch (e) {
        return res.sendStatus(500);
    }
});

app.get("/delete/cache/key/:key", async (req, res) => {
    try {
        const result = await config.redis.del(req.params.key);
        return res.json({ result });
    } catch (e) {
        return res.sendStatus(500);
    }
});

app.get("/redis/cache/keys", async (req, res) => {
    try {
        const keys = await config.redis.keys();
        keys.map(async (k) => {
            await config.redis.del(k);
        });
        return res.json({ keys });
    } catch (e) {
        return res.sendStatus(500);
    }
});

// Middlewares
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Routes
const routes = require("./routes");
routes(app);

app.listen(port);
console.log("[INFO] RESTful API server started on: " + port);
