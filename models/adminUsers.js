const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

mongoose.set("useCreateIndex", true);

const AdminUsers = new Schema(
    {
        email: String,
        name: { type: String, text: true },
        firstname: String,
        lastname: String,
        company: {
            name: String,
            field: String,
            country: String,
            implmentationDate: Date,
            findoctorMails: Boolean,
            contract: Boolean,
            phone: String,
            hubspot: {
                id: String,
                dealId: String,
            },
        },
        role: String,
        password: String,
        tenants: Array,
        test: Boolean,
        legajosApproved: Boolean,
        legajosCompleted: Boolean,
        permissions: [String],
        //news
        username: String,
        verified: { type: Boolean, default: false },
        language: { type: String, default: "es" },
        picture: String,
        phone: String,
        google: {
            id: String,
            domain: String,
        },
        facebook: {
            id: String,
        },
        hubspot: {
            id: String,
        },
    },
    {
        collection: "adminUsers",
    }
);
AdminUsers.index({ name: "text" });
module.exports = mongoose.model("adminUsers", AdminUsers);
