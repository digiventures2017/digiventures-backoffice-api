const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const analyticsSchema = new Schema(
    {
        tenantId: String,
        tableId: String,
        type: {
            type: String,
            default: "legajos"
        }
    },
    {
        collection: "analytics",
        timestamps: { createdAt: "created_at" }
    }
);

module.exports = mongoose.model("analytics", analyticsSchema);
