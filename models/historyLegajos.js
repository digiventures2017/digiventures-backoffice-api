const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

const HistoryLegajos = new Schema(
    {
        legajoId: String,
        userId: String,
        userName: String,
        nameField: String,
        valueField: Schema.Types.Mixed,
        oldValueField: Schema.Types.Mixed,
        totalCompleteOld: Number,
        totalCompleteNew: Number,
    },
    {
        collection: "historyLegajos",
        timestamps: { createdAt: "created_at" },
    }
);

const model = mongoose.model("historyLegajos", HistoryLegajos);
const sincronize = require("../bigquery/bigquery-script");

sincronize(model, "onboarding", {
    searchQuery: { nameField: { $in: ["status", "managment.owner.name"] } },
    predefined: {
        oldValueField: {
            type: "STRING",
            required: false,
        },
        valueField: {
            type: "STRING",
            required: false,
        },
    },
    sort: {
        updatedAt: -1,
    },
    fieldSearchTime: "updatedAt",
});

module.exports = model;
