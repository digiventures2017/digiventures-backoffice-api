#! /bin/bash
echo "Desactivo CI"
unset CI

echo "Comprimo y subo al server"
zip -r /tmp/api-admin.zip . -x node_modules\*
scp -r /tmp/api-admin.zip ubuntu@$DEPLOY_HOST_API_FRONTEND:/home/ubuntu/Projects/api-admin.zip

ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects; sudo rm -rf api-admin; unzip -o api-admin.zip -d api-admin;'

echo "Stash, pull and build"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects/api-admin; sudo docker-compose -f docker-compose.yml up -d --build;'

#echo "Remove zip and folder"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'cd /home/ubuntu/Projects; rm -rf api-admin.zip; rm -rf api-admin;'

echo "Removing orphan images"
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'sudo docker rmi -f $(sudo docker images -a -q --filter "dangling=true")'
ssh ubuntu@$DEPLOY_HOST_API_FRONTEND 'sudo docker rm $(sudo docker ps -a -q --filter status=exited)'

echo "Success!"

exit 0