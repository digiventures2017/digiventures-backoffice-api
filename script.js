const get = require("lodash/get");

const config = {
    selectedFlow: "asd",
    domain: "asd",
    country: "asd",
    palette: {
        buttonText: "#FFFFFF",
        buttonBackground: "#1a88ff",
        navbarText: "#FFFFFF",
        navbarBackground: "#1a88ff",
    },
    favicon: null,
    logo: null,
    bannerImg: null,
    navbarText: "Solicitá tu tarjeta",
    textPrimary:
        "Disfrutá de los mejores descuentos. Sin gastos de otorgamiento",
    textSecondary: "Solicitá tu tarjeta ahora y recibila en tu domicilio.",
    buttonText: "Quiero mi tarjeta de crédito",
    termsAndConditions:
        "ESTE ES UN FLUJO DE PRUEBA Su uso es sólo para la demostración del producto, no tiene ninguna validez legal ni comercial.",
    pageDescription:
        "Solicitar tarjeta de crédito Online. Sin moverte de tu casa y con los mejores beneficios, promociones y descuentos.",
};
const translations = {
    selectedFlow: "Tipo de flujo",
    domain: "Subdominio",
    country: "País",
    palette: "Paleta",
    favicon: "Favicon",
    logo: "Logo",
    bannerImg: "Navbar: logo",
    navbarText: "Navbar: texto",
    textPrimary: "Landing: texto principal",
    textSecondary: "Landing: texto secundario",
    buttonText: "Landing: texto botón siguiente",
    termsAndConditions: "Términos y condiciones",
    pageDescription: "Descripción de la página",
    "palette.buttonBackground": "Paleta: color primario del botón",
    "palette.navbarBackground": "Paleta: color primario del fondo",
    "palette.navbarText": "Paleta: color primario del texto del navbar",
    "palette.buttonText": "Paleta: color primario del texto del botón",
};

function process(data, prefix = "", textPrefix = "") {
    const result = Object.keys(data).map((key) => {
        let value = get(data, key);
        if (value !== null && typeof value === "object") {
            value = process(value, "palette.", "-");
        }
        return `${textPrefix}${translations[`${prefix}${key}`]}: ${value}`;
    });
    return `\n${result.join("\n")}`;
}
